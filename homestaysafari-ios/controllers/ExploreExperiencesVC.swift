//
//  ExploreExperiencesVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 12/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import JGProgressHUD
import MaterialComponents

class ExploreExperiencesViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, BookingDateDelegate {
    
    static let TAG = "ExploreExperienceVC"
    //Dataobjects
    var mostPopularExperiences = [ExperienceListing]()
    var searchResults = [ExperienceListing]()
    var selectedPopExp:ExperienceListing?
    var searchController: UISearchController? = nil
    let locationManager = CLLocationManager()
    var experienceSearchParams = ExperienceSearchParams()
    //Outlets and UI
    let hud = JGProgressHUD(style: .dark)
    @IBOutlet weak var mostPopularWrapperView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loginActionsWrapper: UIStackView!
    @IBOutlet weak var popExperiencesTableView: UITableView!
    @IBOutlet weak var expStartDateWrapper: GradientView!
    @IBOutlet weak var expEndDateWrapper: GradientView!
    @IBOutlet weak var expStartDateNameLabel: UILabel!
    @IBOutlet weak var expStartDateLabel: UILabel!
    @IBOutlet weak var expEndDateNameLabel: UILabel!
    @IBOutlet weak var expEndDateLabel: UILabel!
    
    @IBAction func searchTouchUpInside(_ sender: Any) {
        //check for value. can substitute with guard
        let searchQuery = searchBar.text ?? ""
        if searchQuery != "" {
            experienceSearchParams.query = searchQuery
            //Dates validation
            //
            if(experienceSearchParams.checkinDate?.compare(experienceSearchParams.checkoutDate!) == .orderedSame){
                // Present a modal alert
                let alertController = MDCAlertController(title: "Check Dates", message: "Checkin Date cannot be the same as Checkout date")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
            }
            else if(experienceSearchParams.checkinDate?.compare(experienceSearchParams.checkoutDate!) == .orderedDescending){
                // Present a modal alert
                let alertController = MDCAlertController(title: "Check Dates", message: "Checkin Date cannot be in the future of Checkout date")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
            }
            else if(!Calendar.current.isDate(experienceSearchParams.checkinDate!, inSameDayAs:Date()) && experienceSearchParams.checkinDate?.compare(Date()) == .orderedAscending){
                // Present a modal alert
                let alertController = MDCAlertController(title: "Check Dates", message: "Checkin Date cannot be the in the past")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                present(alertController, animated:true, completion:nil)
            }
            else{
                //Search if validation success
                attemptExperienceSearch()
            }            
        }
        else{
            // Present a modal alert
            let alertController = MDCAlertController(title: "No Text", message: "Please Enter Search Query")
            let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
            alertController.addAction(action)
            //
            present(alertController, animated:true, completion:nil)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        //
        let nib = UINib(nibName: "PopularExperienceUITableViewCell", bundle: nil)
        popExperiencesTableView.register(nib, forCellReuseIdentifier: "PopularExperienceUITableViewCell")
        popExperiencesTableView.dataSource = self
        popExperiencesTableView.delegate = self
        //
        mostPopularWrapperView.isHidden = true
        //
        //Hide Login actions if user logged in
        if !UserDefaultsManager.isloggedin(){
            if let wrapper = loginActionsWrapper{
                wrapper.isHidden = true
            }
        }
        //
        //Location Setup for most popular
        // Request User for Authorisation.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        //
        //Check if Authorized
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        //SetUpDate
        initDates()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Drawer
        setUpDrawer()
    }
    
    
    func initDates(){
        //Add Date Selector Gesture recognizers
        let tapStartDateGesture = UITapGestureRecognizer(target: self, action: #selector(handleStartDateTap(sender:)))
        let tapEndDateGesture = UITapGestureRecognizer(target: self, action: #selector(handleEndDateTap(sender:)))
        //add the gesture recognizers to a views
        expStartDateWrapper.addGestureRecognizer(tapStartDateGesture)
        expEndDateWrapper.addGestureRecognizer(tapEndDateGesture)
        //
        //set date vars and labels
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        expStartDateLabel.text = formatter.string(from: startDate)
        expEndDateLabel.text = formatter.string(from: endDate)
        formatter.dateFormat = "EEEE"
        expStartDateNameLabel.text = formatter.string(from: startDate).uppercased()
        expEndDateNameLabel.text = formatter.string(from: endDate).uppercased()
        //
        //set initial default date
        experienceSearchParams.checkinDate = startDate
        experienceSearchParams.checkoutDate = endDate
    }
    
    func setUpDrawer(){
        if UserDefaultsManager.isloggedin(){
            loginActionsWrapper.isHidden = true
        }
        else{
            loginActionsWrapper.isHidden = false
        }
    }
    
    
    
    @objc func handleStartDateTap(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let datepickercontroller = storyboard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        datepickercontroller.delegate = self
        datepickercontroller.datename = "checkin"
        datepickercontroller.selectedDate = experienceSearchParams.checkinDate!
        self.present(datepickercontroller, animated: true, completion: nil)
    }
    
    @objc func handleEndDateTap(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let datepickercontroller = storyboard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        datepickercontroller.delegate = self
        datepickercontroller.datename = "checkout"
        datepickercontroller.selectedDate = experienceSearchParams.checkoutDate!
        self.present(datepickercontroller, animated: true, completion: nil)
    }
    
    func loadMostPopular(withCordinate latitude:String, longitude:String){
        //mostPopularProgress.isHidden = true
        //load nearby experiences since no popular experience endpoint
        RequestManager.loadNearByExperiences(with:latitude, longitude:longitude){[weak weakSelf = self](status, response, msg) in
            if (status){
                self.mostPopularExperiences.removeAll()
                self.mostPopularExperiences = (response?.data!)!
                //Refresh ui on main thread
                DispatchQueue.main.async {
                    self.popExperiencesTableView.reloadData()
                    self.mostPopularWrapperView.isHidden = false
                }
            }
            else{
                self.mostPopularWrapperView.isHidden = true
            }
        }
    }
    func attemptExperienceSearch(){
        var query = experienceSearchParams.query!
        //define n show progress dialog
        hud.textLabel.text = "Searching for "+query
        hud.show(in: self.view)
        RequestManager.searchExperiences(with:query){[weak weakSelf = self](status, response, msg) in
            //dismiss progress dialog from main thread
            DispatchQueue.main.async {
                self.hud.dismiss()
            }
            
            //check for success
            if (status){
                //Perform Thread on main thread
                DispatchQueue.main.async {
                    self.searchResults = response!.data!
                    self.performSegue(withIdentifier: "toExperienceResultsVCSegue", sender: self)
                }
            }
            else{
                //Define & Present a modal alert
                let message = MDCSnackbarMessage()
                message.text = "Search Failed"
                MDCSnackbarManager.show(message)
                //Define
                let alertController = MDCAlertController(title: "Search failed", message: "Search for "+query+" failed \n "+msg!)
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //Present
                self.present(alertController, animated:true, completion:nil)
            }
        }
    }
    
    
    
    
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "toExperienceResultsVCSegue" {
            let searchQuery = searchBar.text
            if !(searchQuery?.isEmpty)! {
                if self.searchResults.isEmpty{
                    // Present a modal alert
                    let alertController = MDCAlertController(title: "No Results", message: "No Results for Query:\(experienceSearchParams.query)")
                    let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                    alertController.addAction(action)
                    //
                    present(alertController, animated:true, completion:nil)
                    //
                    return false
                }
                else{
                    return true
                }
                
            }
            else{
                // Present a modal alert
                let alertController = MDCAlertController(title: "No Text", message: "Please Enter Search Query")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
                //
                return false
            }
            
        }
        else if identifier == "toViewPopExpSegue"{
            if let experience = selectedPopExp {
                return true
            }
            else{
                return false
            }
        }
        else{
            return true
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toExperienceResultsVCSegue" {
            //Get and cast segue destination
            var experienceResultsVC = segue.destination as! ExperienceResultsViewController
            //Preset data on destionation
            experienceResultsVC.results = searchResults
            experienceResultsVC.searchQuery = searchBar.text ?? ""
            experienceResultsVC.experienceSearchParams = experienceSearchParams
            
        }
        else if segue.identifier == "toViewPopExpSegue" {
            //Get and cast segue destination
            let viewExperienceVC = segue.destination as! ViewExperienceViewController
            //Preset data on destionation
            viewExperienceVC.experience = selectedPopExp!
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        //Load most popular experiences
        loadMostPopular(withCordinate: "\(locValue.latitude)", longitude: "\(locValue.longitude)")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (mostPopularExperiences.count > 10) {
            return 10
        }
        else {
            return mostPopularExperiences.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Load nib
        let cell = Bundle.main.loadNibNamed("PopularExperienceUITableViewCell", owner: self, options: nil)?.first as! PopularExperienceUITableViewCell
        if let image = mostPopularExperiences[indexPath.row].image{
            if let imagelink = image.link{
                //attempt asynchronous image load and caching
                cell.popExpImageView.kf.setImage(with: URL(string: imagelink))
                //cell.imageview.contentMode = .scaleAspectFill
            }
        }
        if let experiencename = mostPopularExperiences[indexPath.row].name{
            cell.popExpNameLabel.text = experiencename
        }
        if let experienceaddress = mostPopularExperiences[indexPath.row].address{
            if let experienceloc = experienceaddress.location{
                cell.popExpLocationLabel.text = experienceloc
            }
            else{
                cell.popExpLocationLabel.text = ""
            }
        }
        else{
            cell.popExpLocationLabel.text = ""
        }
        //
        //add border to cell as spacing hack
        cell.layer.borderWidth = CGFloat(5)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        //honor
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //change variable for segue destination set
        selectedPopExp = mostPopularExperiences[indexPath.row]
        //
        performSegue(withIdentifier: "toViewPopExpSegue", sender: self)
    }
    
    func didSelectDate(date: Date, name: String) {
        print("\(name) : \(date)")
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        if name == "checkin" {
            experienceSearchParams.checkinDate = date
            expStartDateLabel.text = formatter.string(from: date)
            formatter.dateFormat = "EEEE"
            expStartDateNameLabel.text = formatter.string(from: date).uppercased()
        }
        else if name == "checkout"{
            experienceSearchParams.checkoutDate = date
            expEndDateLabel.text = formatter.string(from: date)
            formatter.dateFormat = "EEEE"
            expEndDateNameLabel.text = formatter.string(from: date).uppercased()
        }
    }
    
}
