//
//  RecoverAccountVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 12/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

class RecoverAccountViewController: UIViewController {
    
    @IBOutlet weak var emailInput: MDCTextField!
    var emailInputController: MDCTextInputControllerOutlined?
    @IBAction func recoverBtnTouchUpInside(_ sender: AppleMusicButton) {
        dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        emailInputController = MDCTextInputControllerOutlined(textInput: emailInput)
    }
    
    func recoverAccount(){
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose disposables
    }
}
