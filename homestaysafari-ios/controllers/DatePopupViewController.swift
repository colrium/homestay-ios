//
//  DatePopupViewController.swift
//  homestaysafari-ios
//
//  Created by Collins on 23/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MaterialComponents

class DatePopupViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var okBtn: MDCRaisedButton!
    var delegate:BookingDateDelegate?
    var selectedDate:Date = Date()
    var datename:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.date = selectedDate
    }
    
    @IBAction func ok_touchUpInside(_ sender: Any) {
        //dismiss Popup
        selectedDate = datePicker.date
        delegate?.didSelectDate(date: selectedDate, name:datename)
        dismiss(animated: true)
    }
}
