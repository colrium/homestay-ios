//
//  UserRegistrationVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 31/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MaterialComponents
import JGProgressHUD


class SignUpViewController: UIViewController {
    //
    @IBOutlet weak var firstNameInput: MDCTextField!
    @IBOutlet weak var lastNameInput: MDCTextField!
    @IBOutlet weak var emailInput: MDCTextField!
    @IBOutlet weak var passwordInput: MDCTextField!
    @IBOutlet weak var confirmPasswordInput: MDCTextField!
    @IBOutlet weak var countryInput: MDCTextField!
    @IBOutlet weak var cityInput: MDCTextField!
    @IBOutlet weak var zipCodeInput: MDCTextField!
    @IBOutlet weak var phoneInput: MDCTextField!
    //
    //Material Design Controllers
    var firstNameInputController: MDCTextInputControllerOutlined?
    var lastNameInputController: MDCTextInputControllerOutlined?
    var emailInputController: MDCTextInputControllerOutlined?
    var passwordInputController: MDCTextInputControllerOutlined?
    var confirmPasswordInputController: MDCTextInputControllerOutlined?
    var countryInputController: MDCTextInputControllerOutlined?
    var cityInputController: MDCTextInputControllerOutlined?
    var zipCodeInputController: MDCTextInputControllerOutlined?
    var phoneInputController: MDCTextInputControllerOutlined?
    //Progress indicator
    let hud = JGProgressHUD(style: .light)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // setup
        firstNameInputController = MDCTextInputControllerOutlined(textInput: firstNameInput)
        lastNameInputController = MDCTextInputControllerOutlined(textInput: lastNameInput)
        emailInputController = MDCTextInputControllerOutlined(textInput: emailInput)
        passwordInputController = MDCTextInputControllerOutlined(textInput: passwordInput)
        confirmPasswordInputController = MDCTextInputControllerOutlined(textInput: confirmPasswordInput)
        countryInputController = MDCTextInputControllerOutlined(textInput: countryInput)
        cityInputController = MDCTextInputControllerOutlined(textInput: cityInput)
        zipCodeInputController = MDCTextInputControllerOutlined(textInput: zipCodeInput)
        phoneInputController = MDCTextInputControllerOutlined(textInput: phoneInput)
    }
    
    func attemptUserSignup(){
        //validate inputs
        guard let firstName = firstNameInput.text, firstName.lengthOfBytes(using: .utf8) > 0 else {
            //
            firstNameInputController?.setErrorText("Error: First Name is Required", errorAccessibilityValue: nil)
            // Request Focus on input
            self.firstNameInput.becomeFirstResponder()
            return
        }
        guard let lastName = lastNameInput.text, lastName.lengthOfBytes(using: .utf8) > 0 else {
            //
            lastNameInputController?.setErrorText("Error: Last Name is Required", errorAccessibilityValue: nil)
            // Request Focus on input
            self.lastNameInput.becomeFirstResponder()
            return
        }
        guard let email = emailInput.text, email.lengthOfBytes(using: .utf8) > 0 else {
            //
            emailInputController?.setErrorText("Error: Email is Required", errorAccessibilityValue: nil)
            // Request Focus on input
            self.emailInput.becomeFirstResponder()
            return
        }
        guard let password = passwordInput.text, password.lengthOfBytes(using: .utf8) > 0 else {
            //
            passwordInputController?.setErrorText("Error: Password is Required", errorAccessibilityValue: nil)
            // Request Focus on input
            self.passwordInput.becomeFirstResponder()
            return
        }
        guard let confirmPassword = confirmPasswordInput.text, confirmPassword.lengthOfBytes(using: .utf8) > 0 else {
            //
            confirmPasswordInputController?.setErrorText("Error: Please Re enter password", errorAccessibilityValue: nil)
            // Request Focus on input
            self.confirmPasswordInput.becomeFirstResponder()
            return
        }
        
        if confirmPassword != password {
            //
            passwordInputController?.setErrorText("Error: Passwords do not match", errorAccessibilityValue: nil)
            confirmPasswordInputController?.setErrorText("Error: Passwords do not match", errorAccessibilityValue: nil)
            // Request Focus on input
            self.confirmPasswordInput.becomeFirstResponder()
            return
        }
        //
        //On validation success
        let name = firstName+" "+lastName
        let country = countryInput.text ?? ""
        let city = cityInput.text ?? ""
        let zipCode = zipCodeInput.text ?? ""
        let phone = phoneInput.text ?? ""
        
        
        hud.textLabel.text = "Registering"
        hud.show(in: self.view)
        
        RequestManager.RegisterUser(with: email, password: password, name: name, country: country, town: city, zip: zipCode, phone: phone){[weak weakSelf = self](status, msg, response) in
            DispatchQueue.main.async {
                self.hud.dismiss()
            }
            print("status", status)
            if (status){
                DispatchQueue.main.async {
                    let message = MDCSnackbarMessage()
                    message.text = "Registration successful!"
                    MDCSnackbarManager.show(message)
                    self.performSegue(withIdentifier: "fromSignUpToSignIn", sender: self)
                }
                
            }
            else{
                //print("completionresponse", response)
                //
                var errormessage = ""
                if let errors = response?.errors{
                    errors.forEach({ (arg0)  in
                        for error in arg0.value{
                            errormessage += "\n "+error
                        }
                    })
                }
                DispatchQueue.main.async {
                    let message = MDCSnackbarMessage()
                    message.text = "Registration failed! "+errormessage
                    MDCSnackbarManager.show(message)
                }
                
            }
        }
    }
    
    @IBAction func registerBtnTouchUpInside(_ sender: Any) {
        //ToDo:
        attemptUserSignup()
    }
    
    @IBAction func firstnamechanged(_ sender: Any) {
        firstNameInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    
    @IBAction func lastnamechanged(_ sender: Any) {
        lastNameInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    @IBAction func emailchanged(_ sender: Any) {
        emailInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    
    @IBAction func passwordchanged(_ sender: Any) {
        passwordInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    @IBAction func confirmpasswordchanged(_ sender: Any) {
        confirmPasswordInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
}
