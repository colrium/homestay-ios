//
//  ExperienceResultsVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 19/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD
import MaterialComponents

class ExperienceResultsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    
    let TAG = "ExperienceResultsViewController"
    //Search Parametres
    var experienceSearchParams:ExperienceSearchParams!
    var searchQuery = String()
    var searchStartDate = String()
    var searchEndDate = String()
    //Variables
    let hud = JGProgressHUD(style: .dark)
    var mapMarkers = [MapMarker]()
    var locations = [CLLocationCoordinate2D]()
    var annotations = [MKPointAnnotation]()
    var mapSpan : MKCoordinateSpan?
    var mapRegion : MKCoordinateRegion?
    var results = [ExperienceListing]()
    var selectedExperience : ExperienceListing?
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    //Outlets and UI
    @IBOutlet weak var expResultsHeaderView: UIView!
    @IBOutlet weak var expResultsMapView: MKMapView!
    @IBOutlet weak var expResultsLabel: UILabel!
    @IBOutlet weak var expResultsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        expResultsLabel.text = "Results(\(results.count))"
        //
        //Setup TableView
        expResultsTableView.delegate = self
        expResultsTableView.dataSource = self
        expResultsTableView.tableFooterView = UIView(frame: .zero)
        let nib = UINib(nibName: "ExperienceResultsTableViewCell", bundle: nil)
        expResultsTableView.register(nib, forCellReuseIdentifier: "ExperienceResultsTableViewCell")
        //
        //
        // Setup Map and Markers
        // Ask user for Authorisation to get location
        self.locationManager.requestAlwaysAuthorization()
        //
        self.locationManager.requestWhenInUseAuthorization()
        //
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        //
        setMapMarkers()
        //
        //Setup header Chipviews
        setUpSearchParamsViews()
    }
    func setUpSearchParamsViews(){
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        //
        var chips = [MDCChipView]()
        //Search Query Chip
        let querychipView = MDCChipView()
        querychipView.titleLabel.text = experienceSearchParams.query
        querychipView.setTitleColor(#colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1), for: .normal)
        querychipView.sizeToFit()
        chips.append(querychipView)
        //searchParamsView.addSubview(querychipView)
        //
        //Checkin Query Chip
        let checkinChipView = MDCChipView()
        checkinChipView.titleLabel.text = formatter.string(from: experienceSearchParams.checkinDate!)
        checkinChipView.setTitleColor(#colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), for: .normal)
        //checkinChipView.sizeToFit()
        chips.append(checkinChipView)
        //
        //Checkout Query Chip
        let checkoutChipView = MDCChipView()
        checkoutChipView.titleLabel.text = formatter.string(from: experienceSearchParams.checkoutDate!)
        checkoutChipView.setTitleColor(#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1), for: .normal)
        //checkoutChipView.sizeToFit()
        chips.append(checkoutChipView)
        //
        let sv = UIStackView(arrangedSubviews: chips)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        sv.spacing = 5
        expResultsHeaderView.addSubview(sv)
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.topAnchor.constraint(equalTo: expResultsHeaderView.topAnchor).isActive = true
        sv.bottomAnchor.constraint(equalTo: expResultsHeaderView.bottomAnchor).isActive = true
        sv.rightAnchor.constraint(equalTo: expResultsHeaderView.rightAnchor).isActive = true
        sv.leftAnchor.constraint(equalTo: expResultsHeaderView.leftAnchor).isActive = true
    }
    func setMapMarkers(){
        for experience in self.results{
            if let address = experience.address{
                if let latitude = address.latitude, let longitude = address.longitude{
                    //Define and append new location for experience
                    var location = CLLocationCoordinate2DMake(CLLocationDegrees(latitude)!, CLLocationDegrees(longitude)!)
                    self.locations.append(CLLocationCoordinate2DMake(CLLocationDegrees(latitude)!, CLLocationDegrees(longitude)!))
                    var annotation = MKPointAnnotation()
                    annotation.title = experience.name
                    annotation.coordinate = location
                    annotation.subtitle = experience.description
                    self.annotations.append(annotation)
                    //Span for zoom level
                    self.mapSpan = MKCoordinateSpan.init(latitudeDelta: 0.2, longitudeDelta: 0.2)
                    //
                    self.mapRegion = MKCoordinateRegion(center:location, span:self.mapSpan!)
                }
            }
            
        }
        self.expResultsMapView.addAnnotations(self.annotations)
        if let mapRegion = self.mapRegion {
            self.expResultsMapView.region = mapRegion
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Load nib
        let cell = Bundle.main.loadNibNamed("ExperienceResultsTableViewCell", owner: self, options: nil)?.first as! ExperienceResultsTableViewCell
        //set image
        if let image = results[indexPath.row].image {
            if let imagelink = image.link{
                //attempt asynchronous image load and caching
                cell.expResultImage.kf.setImage(with: URL(string: imagelink))
                //cell.imageview.contentMode = .scaleAspectFill
            }
        }
        //
        //Labels
        if let name = results[indexPath.row].name {
            cell.expResultNameLabel.text = name
        }
        //
        if let address = results[indexPath.row].address {
            if let location = address.location {
                cell.expResultLocationLabel.text = location
            }
            else{
                cell.expResultLocationLabel.text = "Unnamed"
            }
        }
        //
        if let rating = results[indexPath.row].rating {
            var roundedrating = rating.rounded(toPlaces: 2)
            if roundedrating > 0 {
                cell.expResultRatingLabel.text = "\(roundedrating)"
            }
            else{
                cell.expResultRatingLabel.text = "\(Constants.DEFAULT_RATING)"
            }
            
        }
        //
        if let charges = results[indexPath.row].charges {
            if let price = charges[0].amount{
                if price > 0 {
                    cell.expResultPriceLabel.text = charges[0].currency! + " \(price) per ticket"
                }
                else{
                    cell.expResultPriceLabel.text = "FREE"
                }
            }
        }
        else{
            cell.expResultPriceLabel.text = "FREE"
        }
        //
        //add border to cell to mimick spacing
        cell.layer.borderWidth = CGFloat(5)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        //
        //honor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Navigate User to ViewExperienceViewController with Selected Result at row
        selectedExperience = results[indexPath.row]
        performSegue(withIdentifier: "resultsToViewExperienceSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "resultsToViewExperienceSegue"{
            //predefine destination variables before segue is performed
            var experienceVC = segue.destination as! ViewExperienceViewController
            experienceVC.experience = selectedExperience!
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "resultsToViewExperienceSegue"{
            if let experience = selectedExperience {
                return true
            }
            else{
                return false
            }
        }
        else{
            return true
        }
    }
    
    

}
