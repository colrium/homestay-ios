//
//  MyPaymentsVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 03/12/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class MyPaymentsViewController: UITabBarController {

    @IBOutlet weak var menuBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBtn.target = self.revealViewController()
        menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.revealViewController()?.rearViewRevealWidth = 240
    }

}
