//
//  ViewExperienceVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 12/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import MaterialComponents

class ViewExperienceViewController : UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, ExperienceChargeCellDelegate {
    
    //Class Vars
    var experience:ExperienceListing = ExperienceListing()
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    var selectedCharges = [ExperienceCharge : Int]()
    //
    //UIComponents and Outlets
    @IBOutlet weak var expNameLabel: UILabel!
    @IBOutlet weak var expRatingLabel: UILabel!
    @IBOutlet weak var expImageView: UIImageView!
    @IBOutlet weak var expMapView: MKMapView!
    @IBOutlet weak var expAddressLabel: UILabel!
    @IBOutlet weak var expDistanceLabel: UILabel!
    @IBOutlet weak var expCheckinLabel: UILabel!
    @IBOutlet weak var expCheckoutLabel: UILabel!
    @IBOutlet weak var chargesTableView: UITableView!
    @IBOutlet weak var expSelectionsLabel: UILabel!

    //
    var mapSpan : MKCoordinateSpan?
    var mapRegion : MKCoordinateRegion?
    
    @IBAction func bookBtnTouchUpInside(_ sender: Any) {
        if !selectedCharges.isEmpty{
            attemptToCreateBooking()
        }
        else{
            //Define & Present a modal alert
            let message = MDCSnackbarMessage()
            message.text = "No selections made yet"
            MDCSnackbarManager.show(message)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        setUpView()
        //
        //Register rooms table view cells nib
        let nib = UINib(nibName: "ExperienceChargeTableViewCell", bundle: nil)
        chargesTableView.register(nib, forCellReuseIdentifier: "ExperienceChargeTableViewCell")
        chargesTableView.delegate = self
        chargesTableView.dataSource = self
        chargesTableView.tableFooterView = UIView(frame: .zero)
    }
    
    func setUpView(){
        //Labels
        if let name = experience.name {
            expNameLabel.text = name
        }
        else{
            expNameLabel.text = "Unnamed Experience"
        }
        if let rating = experience.rating {
            
            if rating > 0 {
                expRatingLabel.text = "\(rating.rounded(toPlaces: 2))"
            }
            else{
                expRatingLabel.text = "\(Constants.DEFAULT_RATING.rounded(toPlaces: 2))"
            }
        }
        if let image = experience.image {
            if let imagelink = image.link{
                //attempt asynchronous image load and caching
                expImageView.kf.setImage(with: URL(string: imagelink))
            }
        }
        //Address Location and Map Marker if address not nil
        //
        if let address = experience.address {
            //Label
            if let location = address.location {
                expAddressLabel.text = location
            }
            //MapView
            //Add facility map marker
            if let latitude = address.latitude, let longitude = address.longitude{
                let location = CLLocationCoordinate2DMake(CLLocationDegrees(latitude)!, CLLocationDegrees(longitude)!)
                let annotation = MKPointAnnotation()
                annotation.title = experience.name
                annotation.coordinate = location
                annotation.subtitle = experience.description
                mapSpan = MKCoordinateSpan.init(latitudeDelta: 0.2, longitudeDelta: 0.2)
                mapRegion = MKCoordinateRegion(center:location, span:self.mapSpan!)
                expMapView.addAnnotation(annotation)
                if let mapRegion = mapRegion{
                    expMapView.region = mapRegion
                }
            }
        }
    }
    
    func updateTotals(){
        var bookingsTotal: Double = 0
        var totalcurrency = ""
        for (charge, nooftickets) in selectedCharges {
            var chargeTotal = charge.amount! * Double(nooftickets)
            bookingsTotal += chargeTotal
            totalcurrency = charge.currency!
        }
        expSelectionsLabel.text = "Total: \(totalcurrency) \(bookingsTotal) \n for \(selectedCharges.count) items"
        
    }
    
    func getTotalCost()->[String:Double]{
        var totals = [String:Double]()
        for (charge, nooftickets) in selectedCharges {
            var chargeCurrency = charge.currency!
            var totalChargeAmount = charge.amount!*Double(nooftickets)
            if totals[chargeCurrency] == nil{
                totals[chargeCurrency] = totalChargeAmount
            }
        }
        return totals
    }
    
    func attemptToCreateBooking(){
        var experienceBooking:ExperienceBookingListing
        var experienceBookingDetails = ExperienceBookingDetails()
        var totals = getTotalCost()        
        //
        experienceBookingDetails.experience = experience
        print("Charge:: \(totals)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        currentLocation = manager.location?.coordinate
        //get distance to experience location from user location
        if let address = experience.address {
            if let latitude = address.latitude, let longitude = address.longitude{
                var distance = Utilities.distance(lat1: currentLocation!.latitude, lon1: currentLocation!.longitude, lat2: Double(latitude)!, lon2: Double(longitude)!, unit: "M")
                //update UI
                expDistanceLabel.text = "\(distance.rounded(toPlaces: 2)) Miles from your current location"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let charges = experience.charges{
            return charges.count
        }
        else{
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //MARK : Table cell
        let cell = Bundle.main.loadNibNamed("ExperienceChargeTableViewCell", owner: self, options: nil)?.first as! ExperienceChargeTableViewCell
        //
        cell.delegate = self
        if let charge = experience.charges?[indexPath.row] {
            cell.setCharge(charge: charge)
        }
        //
        //add border to cell as spacing hack
        cell.layer.borderWidth = CGFloat(5)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        //honor
        return cell
    }
    
    func didSelectEperienceCharge(charge: ExperienceCharge, count: Int) {
        //
        selectedCharges[charge] = count
        //
        //recalculate totals
        updateTotals()
    }
}
