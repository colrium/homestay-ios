//
//  AllInvoicesVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 27/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class AllInvoicesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var invoicesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        menuBtn.target = self.revealViewController()
        menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
    self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.revealViewController()?.rearViewRevealWidth = 240
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return UITableViewCell()
    }
}
