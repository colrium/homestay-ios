//
//  SearchFacilityVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 17/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD
import MaterialComponents
import Kingfisher
import MapKit

class FacilityResultsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    
    let TAG = "SearchFacilityViewController"
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet var searchParamsView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    //
    var facilitySearchParams:FacilitySearchParams!
    var searchQuery = String()
    var roomOptions = String()
    //
    let hud = JGProgressHUD(style: .dark)
    var mapMarkers = [MapMarker]()
    var locations = [CLLocationCoordinate2D]()
    var annotations = [MKPointAnnotation]()
    var mapSpan : MKCoordinateSpan?
    var mapRegion : MKCoordinateRegion?
    var results = [HospitalityListing]()
    var selectedFacility : HospitalityListing?
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        //Check if parameters are parsed
        
        //
        let nib = UINib(nibName: "SearchResultsFacilityTableViewCell", bundle: nil)
        resultsTableView.register(nib, forCellReuseIdentifier: "SearchResultsFacilityTableViewCell")
        
        //resultsTableView.separatorInset = UIEdgeInsets.zero
        //resultsTableView.layoutMargins = UIEdgeInsets.zero
        
        
        //resultsTableView.estimatedRowHeight = 100
        //resultsTableView.rowHeight = UITableView.automaticDimension
        //resultsTableView.contentInset = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        //
        //
        // Ask user for Authorisation to get location
        self.locationManager.requestAlwaysAuthorization()
        //
        self.locationManager.requestWhenInUseAuthorization()
        //
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        //
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        //
        let resultssize = results.count
        self.resultsLabel.text = "Results("+String(describing: resultssize)+")"
        //
        setMapMarkers()
        //
        //SetUp Search Params Views
        setUpSearchParamsViews()
    }
    
    func setUpSearchParamsViews(){
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        //
        var chips = [MDCChipView]()
        //Search Query Chip
        let querychipView = MDCChipView()
        querychipView.titleLabel.text = facilitySearchParams.query
        querychipView.setTitleColor(#colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1), for: .normal)
        querychipView.sizeToFit()
        chips.append(querychipView)
        //searchParamsView.addSubview(querychipView)
        //
        //Checkin Query Chip
        let checkinChipView = MDCChipView()
        checkinChipView.titleLabel.text = formatter.string(from: facilitySearchParams.checkindate!)
        checkinChipView.setTitleColor(#colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), for: .normal)
        //checkinChipView.sizeToFit()
        chips.append(checkinChipView)
        //
        //Checkout Query Chip
        let checkoutChipView = MDCChipView()
        checkoutChipView.titleLabel.text = formatter.string(from: facilitySearchParams.checkoutdate!)
        checkoutChipView.setTitleColor(#colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1), for: .normal)
        //checkoutChipView.sizeToFit()
        chips.append(checkoutChipView)
        
        
        let sv = UIStackView(arrangedSubviews: chips)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        sv.spacing = 5
        searchParamsView.addSubview(sv)
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.topAnchor.constraint(equalTo: searchParamsView.topAnchor).isActive = true
        sv.bottomAnchor.constraint(equalTo: searchParamsView.bottomAnchor).isActive = true
        sv.rightAnchor.constraint(equalTo: searchParamsView.rightAnchor).isActive = true
        sv.leftAnchor.constraint(equalTo: searchParamsView.leftAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Load nib
        let cell = Bundle.main.loadNibNamed("FacilityResultsTableViewCell", owner: self, options: nil)?.first as! FacilityResultsTableViewCell
        //set image
        if let image = results[indexPath.row].image {
            if let imagelink = image.link{
                //attempt asynchronous image load and caching
                cell.mainImage.kf.setImage(with: URL(string: imagelink))
                //cell.imageview.contentMode = .scaleAspectFill
            }
        }
        //
        //Labels
        if let name = results[indexPath.row].title {
            cell.titleLabel.text = name
        }
        //
        if let location = results[indexPath.row].location {
            cell.addressLabel.text = location
        }
        //
        if let rating = results[indexPath.row].rating {
            let ratingrounded = rating.rounded(toPlaces: 2)
            if ratingrounded > 0 {
                cell.ratingLabel.text = "\(ratingrounded)"
            }
            else{
                cell.ratingLabel.text = "\(Constants.DEFAULT_RATING)"
            }
            
        }
        //
        if let rooms = results[indexPath.row].rooms {
            if let dailyprice = rooms[0].daily_price{
               cell.chargesLabel.text = rooms[0].currency! + " " + dailyprice + " per night"
            }
        }
        //
        //add border to cell as spacing hack
        cell.layer.borderWidth = CGFloat(5)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        //honor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedFacility = results[indexPath.row]
        performSegue(withIdentifier: "toViewFacility", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toViewFacility"{
            //predefine destination variables before segue is performed
            var facilityVC = segue.destination as! ViewFacilityViewController
            facilityVC.facility = selectedFacility!
            facilityVC.facilitySearchParams = facilitySearchParams
        }
    }
    
    func setMapMarkers(){
        for hospitalityListing in self.results{
            if let latitude = hospitalityListing.latitude, let longitude = hospitalityListing.longitude{
                //Define and append new location for facility
                var location = CLLocationCoordinate2DMake(CLLocationDegrees(latitude)!, CLLocationDegrees(longitude)!)
                self.locations.append(CLLocationCoordinate2DMake(CLLocationDegrees(latitude)!, CLLocationDegrees(longitude)!))
                var annotation = MKPointAnnotation()
                annotation.title = hospitalityListing.title
                annotation.coordinate = location
                annotation.subtitle = hospitalityListing.description
                self.annotations.append(annotation)
                //Span for zoom level
                self.mapSpan = MKCoordinateSpan.init(latitudeDelta: 0.2, longitudeDelta: 0.2)
                //
                self.mapRegion = MKCoordinateRegion(center:location, span:self.mapSpan!)
            }
        }
        self.mapView.addAnnotations(self.annotations)
        if let mapRegion = self.mapRegion {
            self.mapView.region = mapRegion
        }
    }

    
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "selectedFacilityId"{
            if let facility = selectedFacility {
                return true
            }
            else {
                return false
            }
        }
        else{
            return true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
    }
}
