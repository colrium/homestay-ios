//
//  ExploreFacilitiesVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 12/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//
import Foundation
import UIKit
import MaterialComponents

import MapKit
import JGProgressHUD

class ExploreFacilitiesViewController : UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, BookingDateDelegate {
    
    
    static let TAG = "ExploreFacilitiesVC"
    //
    let hud = JGProgressHUD(style: .dark)
    var searchController: UISearchController? = nil
    let locationManager = CLLocationManager()
    let pickeroptions = ["1 Room, 1 Adult", "1 Room, 2 Adults", "2 Room, 2 Adults", "More Options.."]
    //
    var facilitySearchParams = FacilitySearchParams()
    var mostPopularFacilities = [HospitalityListing]()
    var selectedPopularFac:HospitalityListing?
    var searchResults = [HospitalityListing]()
    //
    @IBOutlet weak var loginsignupView: UIView!
    //
    @IBOutlet var mostpopfacView: MDCCard!
    @IBOutlet weak var roomOptions: UIPickerView!
    @IBOutlet weak var mostPopularProgress: UIView!
    @IBOutlet weak var mostPopularTableView: UITableView!
    @IBOutlet weak var searchInput: UISearchBar!
    @IBOutlet var openMenuBtn: UIBarButtonItem!
    @IBOutlet weak var drawerUserImage: UIImageViewX!
    @IBOutlet weak var drawerUserNameLabel: UILabel!
    @IBOutlet weak var drawerUserEmailLabel: UILabel!
    @IBOutlet weak var checkinDateLabel: UILabel!
    @IBOutlet weak var startDateWrapper: GradientView!
    @IBOutlet weak var endDateWrapper: GradientView!
    @IBOutlet weak var startDateDayLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateDayLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    
    
    
    @IBAction func searchBtnTouchUpInside(_ sender: Any) {
        //check for value. can substitute with guard
        let searchQuery = searchInput.text ?? ""
        if  searchQuery != "" {
            facilitySearchParams.query = searchQuery
            //Dates validation
            //
            if(facilitySearchParams.checkindate?.compare(facilitySearchParams.checkoutdate!) == .orderedSame){
                // Present a modal alert
                let alertController = MDCAlertController(title: "Check Dates", message: "Checkin date cannot be the same as Checkout date")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
            }
            else if(facilitySearchParams.checkindate?.compare(facilitySearchParams.checkoutdate!) == .orderedDescending){
                // Present a modal alert
                let alertController = MDCAlertController(title: "Check Dates", message: "Checkin date cannot be in the future of Checkout date")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
            }
            else if(!Calendar.current.isDate(facilitySearchParams.checkindate!, inSameDayAs:Date()) && facilitySearchParams.checkindate?.compare(Date()) == .orderedAscending){
                // Present a modal alert
                let alertController = MDCAlertController(title: "Check Dates", message: "Checkin date cannot be the in the past!")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
            }
            else{
                //Search
                attemptFacilitySearch()
            }
        }
        else{
            // Present a modal alert
            let alertController = MDCAlertController(title: "No Text", message: "Please Enter Search Query")
            let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
            alertController.addAction(action)
            //
            present(alertController, animated:true, completion:nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        
        //Rooms picker setup
        roomOptions.delegate = self
        roomOptions.dataSource = self
        //
        //Setup Drawer
        setUpDrawer()
        //
        // Request User for Authorisation.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        //
        //Check if Authorized
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        //
        //Register Most Popular facilities Tableview nib, datasource and delegate
        let nib = UINib(nibName: "PopularFacilityTableViewCell", bundle: nil)
        mostPopularTableView.register(nib, forCellReuseIdentifier: "PopularFacilityTableViewCell")
        mostPopularTableView.delegate = self
        mostPopularTableView.dataSource = self
        //
        //Hide popular facilities view till data is loaded
        mostpopfacView.isHidden = true
        //
        //Initialize Dates
        initDates()
    }

    func initDates(){
        //Add Date Selector Gesture recognizers
        let tapStartDateGesture = UITapGestureRecognizer(target: self, action: #selector(handleStartDateTap(sender:)))
        let tapEndDateGesture = UITapGestureRecognizer(target: self, action: #selector(handleEndDateTap(sender:)))
        //add the gesture recognizers to a views
        startDateWrapper.addGestureRecognizer(tapStartDateGesture)
        endDateWrapper.addGestureRecognizer(tapEndDateGesture)
        //
        //set date vars and labels
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        startDateLabel.text = formatter.string(from: startDate)
        endDateLabel.text = formatter.string(from: endDate)
        formatter.dateFormat = "EEEE"
        startDateDayLabel.text = formatter.string(from: startDate).uppercased()
        endDateDayLabel.text = formatter.string(from: endDate).uppercased()
        //
        //set initial default
        facilitySearchParams.checkindate = startDate
        facilitySearchParams.checkoutdate = endDate
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Drawer
        setUpDrawer()
    }
    
    @objc func handleStartDateTap(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let datepickercontroller = storyboard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        datepickercontroller.delegate = self
        datepickercontroller.datename = "checkin"
        datepickercontroller.selectedDate = facilitySearchParams.checkindate!
        self.present(datepickercontroller, animated: true, completion: nil)
    }
    
    @objc func handleEndDateTap(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let datepickercontroller = storyboard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        datepickercontroller.delegate = self
        datepickercontroller.datename = "checkout"
        datepickercontroller.selectedDate = facilitySearchParams.checkoutdate!
        self.present(datepickercontroller, animated: true, completion: nil)
    }
    
    func setUpDrawer(){
        if UserDefaultsManager.isloggedin(){
            loginsignupView.isHidden = true
        }
        else{
            loginsignupView.isHidden = false
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
        //Load most popular facilities
        loadMostPopular(withCordinate: "\(locValue.latitude)", longitude: "\(locValue.longitude)")
    }
    
    func loadMostPopular(withCordinate latitude:String, longitude:String){
        //mostPopularProgress.isHidden = true
        //load nearby facilities since no popular
        RequestManager.loadNearByFacilities(with:latitude, longitude:longitude){[weak weakSelf = self](status, response, msg) in
            if (status){
                self.mostPopularFacilities.removeAll()
                self.mostPopularFacilities = (response?.data!)!
                //Refresh ui on main thread
                DispatchQueue.main.async {
                    self.mostPopularTableView.reloadData()
                    self.mostpopfacView.isHidden = false
                }
                //
                //print("responseString = \(String(describing: response?.data))")
            }
            else{
                self.mostpopfacView.isHidden = true
            }
        }
    }
    
    func attemptFacilitySearch(){
        var query = facilitySearchParams.query!
        //print("facilitySearchParams::: \(facilitySearchParams)")
        //define n show progress dialog
        //
        hud.textLabel.text = "Searching for "+query
        hud.show(in: self.view)
        RequestManager.searchFacilities(with:facilitySearchParams.query!){[weak weakSelf = self](status, response, msg) in
            //dismiss progress dialog from main thread
            DispatchQueue.main.async {
                self.hud.dismiss()
            }
            //
            //check for success
            //
            if (status){
                //print("responseString = \(String(describing: response))")
                DispatchQueue.main.async {
                    self.searchResults = response!.data!
                    self.performSegue(withIdentifier: "toFacilityResultsVCSegue", sender: self)
                }
            }
            else{
                //Define & Present a modal alert
                let message = MDCSnackbarMessage()
                message.text = "Search Failed"
                MDCSnackbarManager.show(message)
                //Define
                let alertController = MDCAlertController(title: "Search failed", message: "Search for "+query+" failed \n "+msg!)
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //Present
                self.present(alertController, animated:true, completion:nil)
            }
        }
    }
    
    
    
    
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "toFacilityResultsVCSegue" {
            let searchQuery = searchInput.text
            if !(searchQuery?.isEmpty)! {
                
                if searchResults.isEmpty{
                    // Present a modal alert
                    let alertController = MDCAlertController(title: "No Results", message: "No Results for Query:\(facilitySearchParams.query)")
                    let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                    alertController.addAction(action)
                    //
                    present(alertController, animated:true, completion:nil)
                    //
                    return false
                }
                else{
                    return true
                }
            }
            else{
                // Present a modal alert
                let alertController = MDCAlertController(title: "No Text", message: "Please Enter Search Query")
                let action = MDCAlertAction(title:"OK") { (action) in print("OK") }
                alertController.addAction(action)
                //
                present(alertController, animated:true, completion:nil)
                //
                return false
            }
        }
        else if identifier == "toViewPopFacSegue"{
            if let facility = selectedPopularFac {
                return true
            }
            else{
                return false
            }
        }
        else{
            return true
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFacilityResultsVCSegue" {
            //get
            var searchQuery = searchInput.text
            //Get and cast segue destination
            var facilityResultsVC = segue.destination as! FacilityResultsViewController
            //Preset data on destionation
            facilityResultsVC.facilitySearchParams = facilitySearchParams
            facilityResultsVC.searchQuery = searchQuery!
            facilityResultsVC.results = searchResults
        }
        else if segue.identifier == "toViewPopFacSegue" {
            //Get and cast segue destination
            let viewFacilityVC = segue.destination as! ViewFacilityViewController
            //Preset data on destionation
            viewFacilityVC.facility = selectedPopularFac!
        }
    }
    
    
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func  pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickeroptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickeroptions.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            facilitySearchParams.adults = 1
            facilitySearchParams.rooms = 1
        }
        else if row == 1 {
            facilitySearchParams.adults = 2
            facilitySearchParams.rooms = 1
        }
        else if row == 2 {
            facilitySearchParams.adults = 2
            facilitySearchParams.rooms = 2
        }
        else if row == 3 {
            facilitySearchParams.adults = 2
            facilitySearchParams.rooms = 2
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel;
        
        if (pickerLabel == nil){
            pickerLabel = UILabel()
            
            pickerLabel?.font = UIFont(name: "Montserrat", size: 12)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        pickerLabel?.text = pickeroptions[row]
        return pickerLabel!;
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (mostPopularFacilities.count >= 10 ) {
            return 10
        }
        else {
            return mostPopularFacilities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Load nib
        let cell = Bundle.main.loadNibNamed("PopularFacilityTableViewCell", owner: self, options: nil)?.first as! PopularFacilityTableViewCell
        if let image = mostPopularFacilities[indexPath.row].image{
            if let imagelink = image.link{
                //attempt asynchronous image load and caching
                cell.popfacimageview.kf.setImage(with: URL(string: imagelink))
                //cell.imageview.contentMode = .scaleAspectFill
            }
        }
        if let facilityname = mostPopularFacilities[indexPath.row].title{
            cell.popfacnameLabel.text = facilityname
        }
        if let facilityloc = mostPopularFacilities[indexPath.row].location{
            cell.popfacloclabel.text = facilityloc
        }
        //
        //add border to cell as spacing hack
        cell.layer.borderWidth = CGFloat(5)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        //honor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //change variable for segue destination set
        selectedPopularFac = mostPopularFacilities[indexPath.row]
        //
        performSegue(withIdentifier: "toViewPopFacSegue", sender: self)
    }
    
    func didSelectDate(date: Date, name:String) {
        print("\(name) : \(date)")
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        if name == "checkin" {
            facilitySearchParams.checkindate = date
            startDateLabel.text = formatter.string(from: date)
            formatter.dateFormat = "EEEE"
            startDateDayLabel.text = formatter.string(from: date).uppercased()
        }
        else if name == "checkout"{
            facilitySearchParams.checkoutdate = date
            endDateLabel.text = formatter.string(from: date)
            formatter.dateFormat = "EEEE"
            endDateDayLabel.text = formatter.string(from: date).uppercased()
        }
    }
}
