//
//  ExploreTabBarViewController.swift
//  homestaysafari-ios
//
//  Created by Collins on 05/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class ExploreTabBarViewController: UITabBarController, SWRevealNavigationDelegate {
    
    

    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var settingsNavBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //SetUp
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupDrawer()
    }
    func setupDrawer(){
        if UserDefaultsManager.isloggedin(){
            openMenu.isEnabled = true
            openMenu.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            //
            settingsNavBtn.isEnabled = true
            settingsNavBtn.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            //
            openMenu.target = revealViewController()
            openMenu.action = Selector("revealToggle:")
            self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        }
        else{
            openMenu.isEnabled = false
            openMenu.tintColor = UIColor.clear
            //
            settingsNavBtn.isEnabled = false
            settingsNavBtn.tintColor = UIColor.clear
        }
    }
    
    
    func didSelectNavItem(position: Int, section: MainMenuSection) {
        //ToDo:
    }
    
    func didSelectUserAvatar(avatarImageView: UIImageView) {
        //ToDo:
    }
    
}
