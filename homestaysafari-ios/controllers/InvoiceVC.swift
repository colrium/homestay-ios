//
//  InvoiceVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 16/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MaterialComponents

class InvoiceViewController: UIViewController {

    @IBOutlet weak var invoiceDescription: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!
    @IBOutlet weak var couponTextField: MDCTextField!
    @IBOutlet weak var applyCouponBtn: UIButton!
    @IBOutlet weak var stkPhoneInput: MDCTextField!
    @IBOutlet weak var mpesaPhoneInput: MDCTextField!
    @IBOutlet weak var mpesaCodeInput: MDCTextField!
    @IBOutlet weak var mpesaProcessAmountLabel: UILabel!
    
    var couponInputController: MDCTextInputControllerOutlined?
    var stkPhoneInputController: MDCTextInputControllerOutlined?
    var mpesaPhoneInputController: MDCTextInputControllerOutlined?
    var mpesaCodeInputController: MDCTextInputControllerOutlined?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        couponInputController = MDCTextInputControllerOutlined(textInput: couponTextField)
        stkPhoneInputController = MDCTextInputControllerOutlined(textInput: stkPhoneInput)
        mpesaPhoneInputController = MDCTextInputControllerOutlined(textInput: mpesaPhoneInput)
        mpesaCodeInputController = MDCTextInputControllerOutlined(textInput: mpesaCodeInput)
    }
}
