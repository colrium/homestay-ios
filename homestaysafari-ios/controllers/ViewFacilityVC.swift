//
//  ViewFacilityVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 12/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import MaterialComponents

class ViewFacilityViewController: UIViewController, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, FacilityRoomCellDelegate {
    
    
    //
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var facilityImage: UIImageView!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var roomsTableView: UITableView!
    @IBOutlet weak var selectionPriceLabel: UILabel!
    @IBOutlet weak var reserveBtn: MDCRaisedButton!
    //Local Variables
    var facilitySearchParams:FacilitySearchParams?
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    var facility = HospitalityListing()
    var facilityImages = [RecordImage]()
    var selectedRooms:[Room:Int] = [:]
    var reservation:FacilityReservation = FacilityReservation()
    let pickeroptions = ["1 Room, 1 Adult", "1 Room, 2 Adults", "2 Room, 2 Adults", "More Options.."]
    //
    var mapSpan : MKCoordinateSpan?
    var mapRegion : MKCoordinateRegion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        initializeViews()
        //
        // Ask user for Authorisation to get location
        self.locationManager.requestAlwaysAuthorization()
        //
        self.locationManager.requestWhenInUseAuthorization()
        //
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if let images = facility.images{
            self.facilityImages = images
        }
    }
    func initializeViews(){
        //Register rooms table view cells nib
        let nib = UINib(nibName: "RoomsUITableViewCell", bundle: nil)
        roomsTableView.register(nib, forCellReuseIdentifier: "RoomsUITableViewCell")
        //Set rooms tableview delegate and datasource
        roomsTableView.delegate = self
        roomsTableView.dataSource = self
        //
        //Labels
        if let title = facility.title {
            nameLabel.text = title
        }
        if let rating = facility.rating {
            let roundedRating = rating.rounded(toPlaces: 2)
            if roundedRating > 0 {
                ratingLabel.text = "\(rating)"
            }
            else{
                ratingLabel.text = "\(Constants.DEFAULT_RATING)"
            }
        }
        if let image = facility.image {
            if let imagelink = image.link{
                //attempt asynchronous image load and caching
                facilityImage.kf.setImage(with: URL(string: imagelink))
            }
        }
        if let address = facility.location {
            addressLabel.text = address
        }
        //
        //Add facility map marker
        if let latitude = facility.latitude, let longitude = facility.longitude{
            let location = CLLocationCoordinate2DMake(CLLocationDegrees(latitude)!, CLLocationDegrees(longitude)!)
            let annotation = MKPointAnnotation()
            annotation.title = facility.title
            annotation.coordinate = location
            annotation.subtitle = facility.description
            mapSpan = MKCoordinateSpan.init(latitudeDelta: 0.2, longitudeDelta: 0.2)
            mapRegion = MKCoordinateRegion(center:location, span:self.mapSpan!)
            mapView.addAnnotation(annotation)
            if let mapRegion = mapRegion{
                mapView.region = mapRegion
            }
        }
    }
    
    func initializeReservation(){
        if let params = facilitySearchParams {
            reservation.checkinDate = params.checkindate
            reservation.checkoutDate = params.checkindate
            reservation.adults = params.adults
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        currentLocation = manager.location?.coordinate
        //get distance to facility from user location
        let distance = Utilities.distance(lat1: currentLocation!.latitude, lon1: currentLocation!.longitude, lat2: Double(facility.latitude!)!, lon2: Double(facility.longitude!)!, unit: "M")
        //update UI
        distanceLabel.text = "\(distance.rounded(toPlaces: 2)) Miles from your current location"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //Dispose recreatable resources
    }
    
    func getTotalCost()->[String:Double]{
        var totals = [String:Double]()
        for (room, optionIndex) in selectedRooms {
            let roomCurrency = room.currency!
            let totalChargeAmount = Double(room.daily_price!)!
            print("daily_price \(Double(room.daily_price!)!)")
            print("totalChargeAmount :: \(totalChargeAmount)")
            if totals[roomCurrency] == nil{
                totals[roomCurrency] = totalChargeAmount
            }
            else{
                totals[roomCurrency]! += totalChargeAmount
            }
        }
        return totals
    }
    
    func updateCost(){
        var totals = getTotalCost()
        var coststr = "Total price is "
        for (currency, amount) in totals {
            coststr += "\n "+currency+" \(amount)"
            
        }
        print("coststr \(coststr)")
        selectionPriceLabel.text = coststr
    }
    
    func makeReservation(){
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.facilityImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as! ImagesCollectionViewCell
        if let imagelink = facilityImages[indexPath.row].link{
            //attempt asynchronous image load and caching
            cell.imageView.kf.setImage(with: URL(string: imagelink))
            //cell.imageview.contentMode = .scaleAspectFill
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let imagelink = facilityImages[indexPath.row].link{
            //attempt asynchronous image load and caching
            facilityImage.kf.setImage(with: URL(string: imagelink))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let rooms = facility.rooms {
            return facility.rooms!.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //MARK : Table cell
        let cell = Bundle.main.loadNibNamed("RoomsUITableViewCell", owner: self, options: nil)?.first as! RoomsUITableViewCell
        let room = facility.rooms![indexPath.row]
        cell.setRoom(with: room)
        cell.delegate = self
        cell.unitsPickerView.dataSource = self
        cell.unitsPickerView.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //ToDo:
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickeroptions.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return  pickeroptions[row]
    }
    func didSelectRoom(room: Room, optionindex:Int) {
        selectedRooms[room] = optionindex
        updateCost()
    }
    func didUnSelectRoom(room: Room) {
        //
        selectedRooms.removeValue(forKey: room)
        updateCost()
    }
    
   
}
