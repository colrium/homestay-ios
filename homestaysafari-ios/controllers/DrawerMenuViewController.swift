//
//  MenuViewController.swift
//  homestaysafari-ios
//
//  Created by Collins on 23/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit

class DrawerMenuViewController: UIViewController, UITableViewDelegate{
    //
    let userDefaults = UserDefaults.standard
    //
    @IBOutlet weak var drawerUserImage: UIImageView!
    @IBOutlet weak var drawerUserIdentityLabel: UILabel!
    @IBOutlet weak var drawerUserEmailLabel: UILabel!
    @IBOutlet weak var drawerMenuTableView: UITableView!
    
    var dataSource: MainMenuDataSource! // you must keep a reference to the data source!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        drawerMenuTableView.separatorColor = UIColor.clear
        drawerMenuTableView.register(MainMenuViewCell.self, forCellReuseIdentifier: "MainMenuViewCell")
        dataSource = MainMenuDataSource()
        drawerMenuTableView.dataSource = dataSource
        drawerMenuTableView.delegate = self
        drawerMenuTableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        //
        if UserDefaultsManager.isloggedin(){
            drawerUserIdentityLabel.text = userDefaults.string(forKey: "name")
            drawerUserEmailLabel.text = userDefaults.string(forKey: "email")
            
            if let imagelink = userDefaults.string(forKey: "imagelink"){
                if imagelink != "" {
                    drawerUserImage.kf.setImage(with: URL(string: imagelink))
                    drawerUserImage.layer.cornerRadius = drawerUserImage.frame.size.width / 2
                    drawerUserImage.clipsToBounds = true
                }
            }
        }
        else{
            drawerUserIdentityLabel.text = "Not Logged in Yet"
            drawerUserEmailLabel.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //get segue identifier from dataSource sections
        var selecteditemSegueIdentifier = dataSource.sections[indexPath.section].segueIIdentifiers[indexPath.row]
        //Perform
        performSegue(withIdentifier: selecteditemSegueIdentifier, sender: self)
    }
}
