//
//  LoginVC.swift
//  homestaysafari-ios
//
//  Created by Collins on 08/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import JGProgressHUD
import Kingfisher

class LoginViewController : UIViewController, UITextViewDelegate {

    
    @IBOutlet weak var emailInput: MDCTextField!
    @IBOutlet weak var passwordInput: MDCTextField!
    @IBOutlet weak var logoutWrapperView: GradientView!
    @IBOutlet weak var userIdentityLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var logoutButton: AppleMusicButton!
    @IBOutlet weak var userImageView: UIImageViewX!
    
    
    var emailInputController: MDCTextInputControllerOutlined?
    var passwordInputController: MDCTextInputControllerOutlined?
    
    //
    let userDefaults = UserDefaults.standard
    //Progress indicator
    let hud = JGProgressHUD(style: .light)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        emailInputController = MDCTextInputControllerOutlined(textInput: emailInput)
        passwordInputController = MDCTextInputControllerOutlined(textInput: passwordInput)
        //
        updateLayout()
        
    }
    //
    func updateLayout(){
        if userDefaults.bool(forKey: "isloggedin"){
            userIdentityLabel.text = userDefaults.string(forKey: "name")
            userEmailLabel.text = userDefaults.string(forKey: "email")
            
            if let imagelink = userDefaults.string(forKey: "imagelink"){
                if imagelink != "" {
                    userImageView.kf.setImage(with: URL(string: imagelink))
                    userImageView.layer.cornerRadius = userImageView.frame.size.width / 2
                    userImageView.clipsToBounds = true
                }
            }
            logoutWrapperView.isHidden = false
        }
        else{
            logoutWrapperView.isHidden = true
        }
    }
    
    
    func attemptLogin(){
        guard let email = emailInput.text, email.lengthOfBytes(using: .utf8) > 0 else {
            //
            emailInputController?.setErrorText("Email is Required", errorAccessibilityValue: nil)
            // Request Focus on input
            self.emailInput.becomeFirstResponder()
            return
        }
        guard let password = passwordInput.text, password.lengthOfBytes(using: .utf8) > 0 else {
            //
            passwordInputController?.setErrorText("Password is Required", errorAccessibilityValue: nil)
            // Request Focus on input
            self.passwordInput.becomeFirstResponder()
            return
        }
        hud.textLabel.text = "Signing In"
        hud.show(in: self.view)
        
        RequestManager.LoginUser(with: email, password: password){[weak weakSelf = self](status, response, message) in
            if status {
                if let loginresponse = response {
                    if let error = loginresponse.error {
                        DispatchQueue.main.async {
                            self.hud.dismiss()
                            let snackbarMessage = MDCSnackbarMessage()
                            snackbarMessage.text = loginresponse.message!
                            MDCSnackbarManager.show(snackbarMessage)
                            self.updateLayout()
                        }
                    }
                    else{
                        
                        
                        RequestManager.getUserAccount(with: loginresponse.access_token!){[weak weakSelf = self](status, profileresponse, message) in
                            if status {
                                //print(profileresponse)
                                if let profile = profileresponse {
                                    //set session variables
                                    self.userDefaults.set(loginresponse.access_token, forKey: "access_token")
                                    self.userDefaults.set(loginresponse.token_type, forKey: "token_type")
                                    self.userDefaults.set(loginresponse.expires_in, forKey: "expires_in")
                                    self.userDefaults.set(loginresponse.refresh_token, forKey: "refresh_token")
                                    self.userDefaults.set(true, forKey: "isloggedin")
                                    //set profile variables
                                    self.userDefaults.set(profile.name ?? "", forKey: "name")
                                    self.userDefaults.set(profile.email ?? "", forKey: "email")
                                    self.userDefaults.set(profile.phone ?? "", forKey: "phone")
                                    self.userDefaults.set(profile.gender ?? "", forKey: "gender")
                                    self.userDefaults.set(profile.location ?? "", forKey: "location")
                                    self.userDefaults.set(profile.rate ?? 0, forKey: "rate")
                                    self.userDefaults.set(profile.identity ?? "", forKey: "identity")
                                    self.userDefaults.set(profile.status ?? "1", forKey: "status")
                                    self.userDefaults.set(profile.website ?? "", forKey: "website")
                                    self.userDefaults.set(profile.description ?? "", forKey: "description")
                                    self.userDefaults.set(profile.image?.link ?? "", forKey: "imagelink")
                                    //Save
                                    self.userDefaults.synchronize()
                                    //
                                    DispatchQueue.main.async {
                                        self.hud.dismiss()
                                        let snackbarMessage = MDCSnackbarMessage()
                                        snackbarMessage.text = "Login successful!"
                                        MDCSnackbarManager.show(snackbarMessage)
                                        self.updateLayout()
                                    }
                                }
                                
                            }
                            else{
                                //
                                DispatchQueue.main.async {
                                    self.hud.dismiss()
                                    let snackbarMessage = MDCSnackbarMessage()
                                    snackbarMessage.text = "Login Failed!"
                                    MDCSnackbarManager.show(snackbarMessage)
                                    self.updateLayout()
                                }
                            }
                        }
                        
                        
                        
                    }
                    
                }
            }
            else {
                DispatchQueue.main.async {
                    self.hud.dismiss()
                    let snackbarMessage = MDCSnackbarMessage()
                    snackbarMessage.text = "Login failed!"
                    MDCSnackbarManager.show(snackbarMessage)
                    self.updateLayout()
                }
            }
        }
    }
    
    @IBAction func signInTouchUpInside(_ sender: Any) {
        attemptLogin()
    }
    @IBAction func emailEditDidEnd(_ sender: Any) {
        emailInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    @IBAction func passwordEditDidEnd(_ sender: Any) {
        passwordInputController?.setErrorText(nil, errorAccessibilityValue: nil)
    }
    
    @IBAction func logoutBtn(_ sender: AppleMusicButton) {
        self.userDefaults.set("", forKey: "access_token")
        self.userDefaults.set(false, forKey: "isloggedin")
        self.userDefaults.synchronize()
        updateLayout()
    }
    
}
