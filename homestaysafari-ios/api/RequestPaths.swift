//
//  RequestPaths.swift
//  homestaysafari-ios
//
//  Created by Collins on 17/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

struct RequestPaths{
    static let BASE_URL = "https://www.homestaysafari.com/"
    static let exchangeRates = BASE_URL+"api/exchange"
    static let facilities = BASE_URL+"api/facilities"
    static let experiences = BASE_URL+"api/experiences"
    static let login = BASE_URL+"oauth/token"
    static let userprofile = BASE_URL+"api/user"
    static let register = BASE_URL+"api/register"
    static let echo = BASE_URL+"api/echo"
    static let uploadFile = BASE_URL+"api/files"
    static let recoverAccount = BASE_URL+"api/recoverAccount"
    static let validateCode = BASE_URL+"api/validateCode"
    static let resetPassword = BASE_URL+"api/resetPassword"
    static let facilitybooking = BASE_URL+"api/booking/facility"
    static let coupon = BASE_URL+"api/discount-coupons"
    static let invoice = BASE_URL+"api/invoices"
    static let payment = BASE_URL+"api/payment"
}
