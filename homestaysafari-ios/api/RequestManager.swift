//
//  RequestManager.swift
//  homestaysafari-ios
//
//  Created by Collins on 17/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//
import Foundation

class RequestManager{
    static let TAG = "RequestManager"
    var mAccessToken:String?
    var mVAT:Double?
    //MARK : Login
    class func LoginUser(with email:String, password:String, completion:@escaping (_ valid:Bool, _ response:UserLoginResponse?,_ msg:String?) -> Swift.Void){
        let url:NSURL = NSURL(string:RequestPaths.login)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        let body = "{\"password\":\""+password+"\",\"username\":\""+email+"\",\"grant_type\":\""+Constants.GRANT_TYPE+"\",\"client_id\":\""+Constants.CLIENT_ID+"\",\"client_secret\":\""+Constants.CLIENT_SECRET+"\"}"
        //
        let postData:NSData = body.data(using: String.Encoding.nonLossyASCII)! as NSData
        //
        let postLength:NSString = String( postData.length ) as NSString
        //
        request.httpMethod = "POST"
        //
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //
        request.httpBody = postData as Data
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            print(TAG+"responseString = \(String(describing: responseString))")
            do {
                let resp = try JSONDecoder().decode(UserLoginResponse.self, from: data)
                completion(true,resp,"OK")
                
            } catch {
                
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    class func getUserAccount(with token:String, completion:@escaping (_ valid:Bool, _ response:UserAccountResponse?,_ msg:String?) -> Swift.Void){
        let url:NSURL = NSURL(string:RequestPaths.userprofile)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        
        //
        request.httpMethod = "GET"
        //
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(Constants.TOKEN_TYPE+" "+token, forHTTPHeaderField: "Authorization")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            print(TAG+"responseString = \(String(describing: responseString))")
            do {
                let resp = try JSONDecoder().decode(UserAccountResponse.self, from: data)
                completion(true,resp,"OK")
                
            } catch {
                
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    class func RegisterUser(with email:String, password:String, name:String, country:String, town:String, zip:String, phone:String, completion:@escaping (_ status:Bool,_ msg:String?,_ response:SignUpResponse?) -> Swift.Void){
        //
        let url:NSURL = NSURL(string:RequestPaths.register)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        let body = "{\"email\":\""+email+"\",\"password\":\""+password+"\",\"password_confirmation\":\""+password+"\",\"name\":\""+name+"\",\"country\":\""+country+"\",\"town\":\""+town+"\",\"zip\":\""+zip+"\",\"phone\":\""+phone+"\"}"
        print("signuppostbody", body)
        //
        let postData:NSData = body.data(using: String.Encoding.nonLossyASCII)! as NSData
        //
        let postLength:NSString = String( postData.length ) as NSString
        //
        request.httpMethod = "POST"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json:charset=utf-8",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = postData as Data
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,"Connection to server failed.", nil) //
                return
            }
            print("data", data)
            if let responsestatus = response as? HTTPURLResponse{
                print(TAG+"Error statusCode  ::  \(responsestatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                do {
                    let resp = try JSONDecoder().decode(SignUpResponse.self, from: data)
                    //
                    completion(false,resp.message, resp)
                } catch {
                    print("JSON DESERIALIZATION ERROR : \(error)")
                    completion(false,"Server could not complete the request", nil)
                }
                
            }
            else{
                //let responseString = String(data: data, encoding: .utf8)
                //print(TAG+"responseString = \(String(describing: responseString))")
                do {
                    let resp = try JSONDecoder().decode(SignUpResponse.self, from: data)
                    //
                    completion(true,resp.message, resp)
                } catch {
                    print("JSON DESERIALIZATION ERROR : \(error)") //
                    //
                    do{
                        let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                        completion(false,svrError.Message, nil)
                    }catch{
                        print("JSON DECODER ERROR : \(error)") //
                        completion(false,"BAD DATA RECEIVED.", nil)
                    }
                }
            }
            
        }
        task.resume()
    }
    //MARK : Recover Account
    class func RecoverAccount(with email:String, completion:@escaping (_ valid:Bool,_ msg:String?) -> Swift.Void){
        
        let url:NSURL = NSURL(string:RequestPaths.recoverAccount)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        let body = "{\"email\":\""+email+"\"}"
        //
        let postData:NSData = body.data(using: String.Encoding.nonLossyASCII)! as NSData
        //
        let postLength:NSString = String( postData.length ) as NSString
        //
        request.httpMethod = "POST"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(AccountRecoveryResponse.self, from: data)
                //
                completion(true,resp.message)
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    //MARK : LoadNearby Facilities
    class func loadNearByFacilities(with latitude:String, longitude:String, completion:@escaping (_ valid:Bool,_ facilities:HospitalityResponse?,_ msg:String?) -> Swift.Void){
        let fullUrl = RequestPaths.facilities+"?loc="+latitude+","+longitude
        let url:NSURL = NSURL(string:fullUrl)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        request.httpMethod = "GET"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            //print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(HospitalityResponse.self, from: data)
                //
                completion(true,resp,"OK")
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    //MARK : Load Popular Facilities
    class func loadPopularFacilities(with latitude:String, longitude:String, completion:@escaping (_ valid:Bool,_ response:HospitalityResponse?,_ msg:String?) -> Swift.Void){
        let fullUrl = RequestPaths.facilities+"?loc="+latitude+","+longitude
        
        let url:NSURL = NSURL(string:fullUrl)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        request.httpMethod = "GET"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            //print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(HospitalityResponse.self, from: data)
                //
                completion(true,resp,"OK")
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    //MARK : Search Facilities
    class func searchFacilities(with keyword:String, completion:@escaping (_ valid:Bool,_ response:HospitalityResponse?,_ msg:String?) -> Swift.Void){
        //
        let fullUrl = RequestPaths.facilities+"?destination="+keyword.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        //TODO : escape url
        
        //
        let url:NSURL = NSURL(string:fullUrl)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        request.httpMethod = "GET"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            //print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(HospitalityResponse.self, from: data)
                //
                completion(true,resp,"OK")
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    //MARK : LoadNearby Experiences
    class func loadNearByExperiences(with latitude:String, longitude:String, completion:@escaping (_ valid:Bool,_ facilities:ExperienceResponse?,_ msg:String?) -> Swift.Void){
        let fullUrl = RequestPaths.experiences+"?loc="+latitude+","+longitude
        let url:NSURL = NSURL(string:fullUrl)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        request.httpMethod = "GET"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            //let responseString = String(data: data, encoding: .utf8)
            //print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(ExperienceResponse.self, from: data)
                //
                completion(true,resp,"OK")
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    //MARK : Load Popular Experiences
    class func loadPopularExperiences(with latitude:String, longitude:String, completion:@escaping (_ valid:Bool,_ response:ExperienceResponse?,_ msg:String?) -> Swift.Void){
        let fullUrl = RequestPaths.experiences+"?loc="+latitude+","+longitude
        let url:NSURL = NSURL(string:fullUrl)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        request.httpMethod = "GET"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(ExperienceResponse.self, from: data)
                //
                completion(true,resp,"OK")
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    //MARK : Search Facilities
    class func searchExperiences(with keyword:String, completion:@escaping (_ valid:Bool,_ response:ExperienceResponse?,_ msg:String?) -> Swift.Void){
        //
        let fullUrl = RequestPaths.experiences+"?destination="+keyword.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        //TODO : escape url
        
        //
        let url:NSURL = NSURL(string:fullUrl)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //
        request.httpMethod = "GET"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,nil,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,nil,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            //print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(ExperienceResponse.self, from: data)
                //
                completion(true,resp,"OK")
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,nil,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,nil,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    func getAccessToken()->String{
        return mAccessToken!
    }
    
    func getVAT()->Double{
        return mVAT!
    }
    
    func setVAT(value:Double){
        mVAT = value
    }
    
    class func createFacilityReservation(with reservation:FacilityReservation, completion:@escaping (_ valid:Bool,_ msg:String?) -> Swift.Void){
        //
        let url:NSURL = NSURL(string:RequestPaths.register)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //let body = "{\"email\":\""+email+"\",\"password\":\""+password+"\",\"name\":\""+name+"\",\"country\":\""+country+"\",\"town\":\""+town+"\",\"zip\":\""+zip+"\",\"phone\":\""+phone+"\"}"
        //
        //let postData:NSData = body.data(using: String.Encoding.nonLossyASCII)! as NSData
        //
        //let postLength:NSString = String( postData.length ) as NSString
        //
        request.httpMethod = "POST"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json:charset=utf-8",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(ReservationResponse.self, from: data)
                //
                completion(true,resp.message)
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }
    
    
    class func createExperienceBooking(with reservation:FacilityReservation, completion:@escaping (_ valid:Bool,_ msg:String?) -> Swift.Void){
        //
        let url:NSURL = NSURL(string:RequestPaths.register)!
        //
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        //let body = "{\"email\":\""+email+"\",\"password\":\""+password+"\",\"name\":\""+name+"\",\"country\":\""+country+"\",\"town\":\""+town+"\",\"zip\":\""+zip+"\",\"phone\":\""+phone+"\"}"
        //
        //let postData:NSData = body.data(using: String.Encoding.nonLossyASCII)! as NSData
        //
        //let postLength:NSString = String( postData.length ) as NSString
        //
        request.httpMethod = "POST"
        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json:charset=utf-8",forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        //Queue
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            //
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false,"Connection to server failed.") //
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print(TAG+"Wrong statusCode  ::  \(httpStatus.statusCode)")
                print(TAG+"response = \(String(describing: response))")
                completion(false,"Server could not complete the request") //
            }
            let responseString = String(data: data, encoding: .utf8)
            print(TAG+"responseString = \(String(describing: responseString))")
            
            do {
                let resp = try JSONDecoder().decode(ReservationResponse.self, from: data)
                //
                completion(true,resp.message)
            } catch {
                print("JSON DESERIALIZATION ERROR : \(error)") //
                //
                do{
                    let svrError = try JSONDecoder().decode(ResponseError.self, from: data)
                    completion(false,svrError.Message)
                }catch{
                    print("JSON DECODER ERROR : \(error)") //
                    completion(false,"BAD DATA RECEIVED.")
                }
            }
        }
        task.resume()
    }



}
