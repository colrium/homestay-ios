//
//  Responses.swift
//  homestaysafari-ios
//
//  Created by Collins on 08/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

struct ResponseError:Decodable {
    var ResponseCode:String?
    var Message :String?
    enum CodingKeys: String, CodingKey {
        case ResponseCode = "reponsecode"
        case Message = "message"
    }
}

struct AccountRecoveryResponse:Decodable {
    var email : String?
    var token:String?
    var error:String?
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case email = "email"
        case token = "token"
        case error = "error"
        case message = "message"
    }
}

struct Amenities :Decodable {
    var id : Int?
    var name:String?
    var description:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
    }
}
struct BookingDetailsListing:Decodable {
    var from:String?
    var to:String?
    var duration:Int?
    var adults:Int?
    var children:Int?
    var facility:BookingFacilityListing?
    var rooms:[BookingRoomListing]?
    
    enum CodingKeys: String, CodingKey {
        case from = "from"
        case to = "to"
        case duration = "duration"
        case adults = "adults"
        case children = "children"
        case facility = "facility"
        case rooms = "rooms"
    }
}
struct BookingFacilityListing:Decodable {
    var id : Int?
    var title:String?
    var location:String?
    var latitude:Double?
    var longitude:Double?
    var country:String?
    var county:String?
    var checkin:String?
    var checkout:Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case location = "location"
        case latitude = "latitude"
        case longitude = "longitude"
        case country = "errors"
        case county = "county"
        case checkin = "checkin"
        case checkout = "checkout"
    }
}
struct BookingListing:Decodable {
    var id:Int?
    var amount:Double?
    var currency:String?
    var status:Int?
    var description:String?
    var type:String?
    var created_at:String?
    var details:BookingDetailsListing?
    var invoice:InvoiceListing?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case amount = "amount"
        case currency = "currency"
        case status = "status"
        case description = "description"
        case type = "type"
        case created_at = "created_at"
        case details = "details"
        case invoice = "invoice"
    }
}
struct BookingRoomListing:Decodable {
    var room_id:Int?
    var currency:String?
    var quantity:Int?
    var amount:Double?
    var adults:Int?
    var children:Int?
    var status:Int?
    
    enum CodingKeys: String, CodingKey {
        case room_id = "room_id"
        case currency = "currency"
        case quantity = "quantity"
        case amount = "amount"
        case adults = "adults"
        case children = "children"
        case status = "status"
    }
}
struct BookingUserListing:Decodable {
    var id:Int?
    var name:String?
    var email:String?
    var phone:String?
    var gender:String?
    var location:String?
    var rate:Double?
    var identity:String?
    var from:Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case gender = "description"
        case location = "location"
        case rate = "rate"
        case identity = "identity"
        case from = "from"
    }
}
struct CheckoutResponse:Decodable {
    var id:Int?
    var status:Int?
    var reference:String?
    var date:String?
    var currency:String?
    var amount:Double?
    var redirect_url:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case status = "status"
        case reference = "reference"
        case date = "date"
        case currency = "currency"
        case amount = "amount"
        case redirect_url = "redirect_url"
    }
}
struct CouponResponse:Decodable {
    var id:Int?
    var title:String?
    var code:String?
    var amount:Double?
    var type:String?
    var value:Double?
    var expiry:String?
    var start:String?
    var status:String?
    var description:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case code = "code"
        case amount = "amount"
        case type = "type"
        case value = "value"
        case expiry = "expiry"
        case start = "start"
        case status = "status"
        case description = "description"
    }
}
struct CurrencyListing:Decodable {
    var id:Int?
    var country:String?
    var code:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case country = "country"
        case code = "code"
    }
}
struct ExchangeRateListing:Decodable {
    var id:Int?
    var country:String?
    var code:String?
    var value:CLong?
    var base:String?
    var timestamp:CLong?
    var description:String?
    var created_at:String?
    var updated_at:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case country = "country"
        case code = "code"
        case value = "value"
        case base = "base"
        case timestamp = "timestamp"
        case description = "description"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
}
struct Experience:Decodable {
    var id:Int?
    var name:String?
    var admission:String?
    var verified:Int?
    var status:Int?
    var rating:Int?
    var description:String?
    var address:RecordAddress?
    var type:ExperienceType?
    var image:ExperienceImage?
    var images:[ExperienceImage]?
    var charges:[ExperienceCharge]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case admission = "admission"
        case verified = "verified"
        case status = "status"
        case rating = "rating"
        case description = "description"
        case address = "address"
        case type = "type"
        case image = "image"
        case images = "images"
        case charges = "charges"
    }
}

struct ExperienceBookingListing:Decodable {
    var id:Int?
    var amount:Double?
    var currency:String?
    var description:String?
    var created_at:String?
    var type:String?
    var details:ExperienceBookingDetails?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case amount = "amount"
        case currency = "currency"
        case description = "description"
        case created_at = "created_at"
        case type = "type"
        case details = "details"
    }
}
struct ExperienceBookingCharge:Decodable {
    var id:Int?
    var name:String?
    var type:String?
    var currency:String?
    var amount:Double?
    var description:String?
    var ticket:ExperienceBookingTicket?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case type = "type"
        case currency = "currency"
        case amount = "amount"
        case description = "description"
        case ticket = "ticket"
    }
}
struct ExperienceBookingDetails:Decodable {
    var date:String?
    var charges:[ExperienceBookingCharge]?
    var experience:ExperienceListing?
    
    enum CodingKeys: String, CodingKey {
        case date = "date"
        case charges = "charges"
        case experience = "experience"
    }
}

struct ExperienceBookingResponse:Decodable {
    var current_page:Int?
    var data:[ExperienceListing]?
    var from:Int?
    var last_page:Int?
    var per_page:Int?
    var to:Int?
    var total:Int?
    
    enum CodingKeys: String, CodingKey {
        case current_page = "current_page"
        case data = "data"
        case from = "from"
        case last_page = "last_page"
        case per_page = "per_page"
        case to = "to"
        case total = "total"
    }
}
struct ExperienceBookingTicket:Decodable {
    var booking_id:Int?
    var charge_id:Int?
    var amount:String?
    var currency:String?
    var quantity:Int?
    var status:Int?
    var description:String?
    
    enum CodingKeys: String, CodingKey {
        case booking_id = "booking_id"
        case charge_id = "charge_id"
        case amount = "amount"
        case currency = "currency"
        case quantity = "quantity"
        case status = "status"
        case description = "description"
    }
}
struct ExperienceCharge:Decodable, Hashable {
    var id:Int?
    var name:String?
    var type:String?
    var currency:String?
    var amount:Double?
    var description:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case type = "type"
        case currency = "currency"
        case amount = "amount"
        case description = "description"
    }
}
struct ExperienceImage:Decodable {
    var id:Int?
    var path:String?
    var description:String?
    var link:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case path = "path"
        case description = "description"
        case link = "link"
    }
}
struct ExperienceInvoiceListing:Decodable {
    var booking:ExperienceBookingListing?
    
    enum CodingKeys: String, CodingKey {
        case booking = "booking"
    }
}
struct ExperienceInvoicingListing:Decodable {
    var id:Int?
    var number:String?
    var currency:String?
    var amount:Double?
    var description:String?
    var created_at:String?
    var due:Double?
    var booking:ExperienceBookingListing?
    var user:InvoiceUserListing?
    var payments:[InvoicePaymentListing]?
    var message:String?
    var error:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case number = "number"
        case currency = "currency"
        case amount = "amount"
        case description = "description"
        case created_at = "created_at"
        case due = "due"
        case booking = "booking"
        case user = "user"
        case payments = "payments"
        case message = "message"
        case error = "error"
    }
}
struct ExperienceListing:Decodable {
    var id:Int?
    var name:String?
    var admission:String?
    var verified:Int?
    var status:Int?
    var tags:String?
    var rating:Double?
    var description:String?
    var address:RecordAddress?
    var type:ExperienceType?
    var image:ExperienceImage?
    var charges:[ExperienceCharge]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case admission = "admission"
        case verified = "verified"
        case status = "status"
        case tags = "tags"
        case rating = "rating"
        case description = "description"
        case address = "address"
        case type = "type"
        case image = "image"
        case charges = "charges"
    }
}
struct ExperienceResponse:Decodable {
    var current_page:Int?
    var data:[ExperienceListing]?
    var from:Int?
    var last_page:Int?
    var per_page:Int?
    var to:Int?
    var total:Int?
    
    enum CodingKeys: String, CodingKey {
        case current_page = "current_page"
        case data = "data"
        case from = "from"
        case last_page = "last_page"
        case per_page = "per_page"
        case to = "to"
        case total = "total"
    }
}
struct ExperienceType:Decodable {
    var id:Int?
    var name:String?
    var description:String?
    var children:[ExperienceType]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case children = "children"
    }
}
struct HospitalityListing:Decodable {
    var id:Int?
    var type:HospitalityFacilityType?
    var title:String?
    var longitude:String?
    var latitude:String?
    var country:String?
    var county:String?
    var description:String?
    var rating:Double?
    var image:RecordImage?
    var amenities:[Amenities]?
    var location:String?
    var images:[RecordImage]?
    var rooms:[Room]?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case title = "title"
        case longitude = "longitude"
        case latitude = "latitude"
        case country = "country"
        case county = "county"
        case description = "description"
        case rating = "rating"
        case image = "image"
        case amenities = "amenities"
        case location = "location"
        case images = "images"
        case rooms = "rooms"
    }
}
struct HospitalityResponse:Decodable {
    var current_page:Int?
    var data:[HospitalityListing]?
    var from:Int?
    var last_page:Int?
    var per_page:Int?
    var to:Int?
    var total:Int?
    
    enum CodingKeys: String, CodingKey {
        case current_page = "current_page"
        case data = "data"
        case from = "from"
        case last_page = "last_page"
        case per_page = "per_page"
        case to = "to"
        case total = "total"
    }
}
struct HospitalityFacilityType:Decodable {
    var id:Int?
    var name:String?
    var description:String?
    var created_at:String?
    var updated_at:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
}
struct RecordImage:Decodable,Equatable,Hashable {
    var id:Int?
    var description:String?
    var link:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case description = "description"
        case link = "link"
    }
}
struct InvoiceListing:Decodable {
    var id:Int?
    var number:String?
    var currency:String?
    var amount:Double?
    var description:String?
    var created_at:Double?
    var due:Double?
    var user:InvoiceUserListing?
    var payments:[InvoicePaymentListing]?
    var message:String?
    var error:String?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case number = "number"
        case currency = "currency"
        case amount = "amount"
        case description = "description"
        case created_at = "created_at"
        case due = "due"
        case user = "user"
        case payments = "payments"
        case message = "message"
        case error = "error"
    }
}
struct InvoicePaymentListing:Decodable {
    var id:Int?
    var number:String?
    var reference:String?
    var amount:Double?
    var currency:String?
    var type:String?
    var date:String?
    var status:Int?
    var description:String?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case number = "number"
        case reference = "reference"
        case amount = "amount"
        case currency = "currency"
        case type = "type"
        case date = "date"
        case status = "status"
        case description = "description"
    }
}
struct InvoicesResponse:Decodable {
    var booking_id:Int?
    var count:String?
    var invoices:[InvoiceListing]?
    
    enum CodingKeys: String, CodingKey {
        case booking_id = "booking_id"
        case count = "count"
        case invoices = "invoices"
    }
}
struct InvoiceUserListing:Decodable {
    var id:Int?
    var name:String?
    var email:String?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
    }
}


struct PaymentResponse:Decodable {
    var type:String?
    var reference:String?
    var date:String?
    var amount:Double?
    var id:Int?
    var number:String?
    var description:String?
    var redirect_url:String?
    var message:String?
    
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case reference = "reference"
        case date = "date"
        case amount = "amount"
        case id = "id"
        case number = "number"
        case description = "description"
        case redirect_url = "redirect_url"
        case message = "message"
    }
}
struct ProfileResponse:Decodable {
    var id:Int?
    var email:String?
    var name:String?
    var gender:String?
    var phone:String?
    var location:String?
    var rate:Double?
    var identity:String?
    var type:String?
    var status:Int?
    var verification:String?
    var created_at:String?
    var updated_at:String?
    var image:RecordImage?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case email = "email"
        case name = "name"
        case gender = "gender"
        case phone = "phone"
        case location = "location"
        case rate = "rate"
        case identity = "identity"
        case type = "type"
        case status = "status"
        case verification = "verification"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case image = "image"
    }
}
struct RatingResponse:Decodable {
    var rating:Int?
    var content:String?
    var date:String?
    var id:Int?
    var facility_id:String?
    
    enum CodingKeys: String, CodingKey {
        case rating = "rating"
        case content = "content"
        case date = "date"
        case id = "id"
        case facility_id = "facility_id"
    }
}
struct RecordAddress:Decodable {
    var id:Int?
    var name:String?
    var email:String?
    var phone:String?
    var room:String?
    var building:String?
    var street:String?
    var town:String?
    var county:String?
    var post:String?
    var country:String?
    var website:String?
    var description:String?
    var location:String?
    var latitude:String?
    var longitude:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case room = "room"
        case building = "building"
        case street = "street"
        case town = "town"
        case county = "county"
        case post = "post"
        case country = "country"
        case website = "website"
        case description = "description"
        case location = "location"
        case latitude = "latitude"
        case longitude = "longitude"
    }
}
struct ReservationInvoiceListing:Decodable {
    var id:Int?
    var number:String?
    var currency:String?
    var amount:Double?
    var description:String?
    var created_at:Double?
    var due:Double?
    var user:InvoiceUserListing?
    var payments:[InvoicePaymentListing]?
    var message:String?
    var error:String?
    var booking:BookingListing?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case number = "number"
        case currency = "currency"
        case amount = "amount"
        case description = "description"
        case created_at = "created_at"
        case due = "due"
        case user = "user"
        case payments = "payments"
        case message = "message"
        case error = "error"
        case booking = "booking"
    }
}
struct ReservationResponse:Decodable {
    var id:Int?
    var created_at:String?
    var from:CLong?
    var to:CLong?
    var status:String?
    var errors:String?
    var message:String?
    var facility:BookingFacilityListing?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case created_at = "created_at"
        case from = "from"
        case to = "to"
        case status = "status"
        case errors = "errors"
        case message = "message"
        case facility = "facility"
    }
}
struct ReservationsResponse:Decodable {
    var current_page:Int?
    var data:[BookingListing]?
    var from:Int?
    var last_page:Int?
    var per_page:Int?
    var to:Int?
    var total:Int?
    
    enum CodingKeys: String, CodingKey {
        case current_page = "current_page"
        case data = "data"
        case from = "from"
        case last_page = "last_page"
        case per_page = "per_page"
        case to = "to"
        case total = "total"
    }
}
struct ResetPasswordResponse:Decodable {
    var email:String?
    var state:Int?
    var error:String?
    var message:String?
    
    enum CodingKeys: String, CodingKey {
        case email = "email"
        case state = "state"
        case error = "error"
        case message = "message"
    }
}
struct ReviewListing:Decodable {
    var review_id:Int?
    
    enum CodingKeys: String, CodingKey {
        case review_id = "review_id"
    }
}
struct ReviewResponse:Decodable {
    var facility_id:String?
    var review:Int?
    var date:String?
    var status:String?
    var message:String?
    
    enum CodingKeys: String, CodingKey {
        case facility_id = "facility_id"
        case review = "review"
        case date = "date"
        case status = "status"
        case message = "message"
    }
}
struct ReviewsResponse:Decodable {
    var facility_id:String?
    var room_id:Int?
    var reviews:ReviewListing?
    var count:String?
    
    enum CodingKeys: String, CodingKey {
        case facility_id = "facility_id"
        case room_id = "room_id"
        case reviews = "reviews"
        case count = "count"
    }
}
struct Room:Hashable, Decodable, Equatable {
    static func == (lhs: Room, rhs: Room) -> Bool {
        return lhs.id == rhs.id
        && lhs.title == rhs.title
        && lhs.type == rhs.type
        && lhs.quantity == rhs.quantity
        && lhs.currency == rhs.currency
        && lhs.bathrooms == rhs.bathrooms
        && lhs.beds == rhs.beds
        && lhs.daily_price == rhs.daily_price
        && lhs.weekly_price == rhs.weekly_price
        && lhs.monthly_price == rhs.monthly_price
        && lhs.description == rhs.description
        && lhs.image == rhs.image
    }
    
    var id:Int?
    var title:String?
    var type:String?
    var quantity:Int?
    var currency:String?
    var bathrooms:Int?
    var beds:Int?
    var daily_price:String?
    var weekly_price:String?
    var monthly_price:String?
    var description:String?
    var image:RecordImage?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case type = "type"
        case currency = "currency"
        case quantity = "quantity"
        case bathrooms = "bathrooms"
        case beds = "beds"
        case daily_price = "daily_price"
        case weekly_price = "weekly_price"
        case monthly_price = "monthly_price"
        case description = "description"
        case image = "image"
    }
}
struct SendInvoiceResponse:Decodable {
    var invoice_id:String?
    var booking_id:Int?
    
    enum CodingKeys: String, CodingKey {
        case invoice_id = "invoice_id"
        case booking_id = "booking_id"
    }
}
struct STKPushResponse:Decodable {
    var merchReqId:String?
    var checkoutReqId:Int?
    var responseCode:Int?
    var responseDesc:String?
    var customerMessage:String?
    
    enum CodingKeys: String, CodingKey {
        case merchReqId = "merchReqId"
        case checkoutReqId = "checkoutReqId"
        case responseCode = "responseCode"
        case responseDesc = "responseDesc"
        case customerMessage = "customerMessage"
    }
}
struct SignUpResponse:Decodable {
    var id:Int?
    var name:String?
    var email:String?
    var phone:String?
    var gender:String?
    var identity:String?
    var description:String?
    var status:Int?
    var message:String?
    var errors:[String:[String]]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case gender = "gender"
        case identity = "identity"
        case description = "description"
        case status = "status"
        case message = "message"
        case errors = "errors"
    }
}

struct ThumbnailImage:Decodable {
    var id:Int?
    var link:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case link = "link"
    }
}

struct UserAccountResponse:Decodable {
    var id:Int?
    var name:String?
    var email:String?
    var phone:String?
    var gender:String?
    var location:String?
    var rate:Int?
    var identity:String?
    var status:String?
    var website:String?
    var description:String?
    var image:RecordImage?
    var company:String?
    var address:RecordAddress?
    var error:String?
    var message:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case gender = "gender"
        case location = "location"
        case rate = "rate"
        case identity = "identity"
        case status = "status"
        case website = "website"
        case description = "description"
        case image = "image"
        case company = "company"
        case address = "address"
        case error = "error"
        case message = "message"
    }
}





struct UserLoginResponse:Decodable {
    var token_type:String?
    var expires_in:CLong?
    var access_token:String?
    var refresh_token:String?
    var error:String?
    var message:String?
    
    enum CodingKeys: String, CodingKey {
        case token_type = "token_type"
        case expires_in = "expires_in"
        case access_token = "access_token"
        case refresh_token = "refresh_token"
        case error = "error"
        case message = "message"
    }
}
