//
//  PopularExperienceUITableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 19/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class PopularExperienceUITableViewCell: UITableViewCell {

    @IBOutlet weak var popExpImageView: UIImageView!    
    @IBOutlet weak var popExpFavImage: UIImageView!
    @IBOutlet weak var popExpNameLabel: UILabel!    
    @IBOutlet weak var popExpLocationLabel: UILabel!
    
    
}
