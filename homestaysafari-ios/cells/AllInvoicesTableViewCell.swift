//
//  AllInvoicesTableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 27/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class AllInvoicesTableViewCell: UITableViewCell {

    @IBOutlet weak var facilityImageView: UIImageView!
    @IBOutlet weak var facilityNameLabel: UILabel!
    @IBOutlet weak var invoiceStatusImageView: UIImageView!
    @IBOutlet weak var invoiceStatusLabel: UILabel!
    @IBOutlet weak var checkinDateLabel: UILabel!
    @IBOutlet weak var checkoutLabel: UILabel!
    
    
}
