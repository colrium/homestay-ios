//
//  RoomsUITableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 24/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MaterialComponents

class RoomsUITableViewCell: UITableViewCell {
    var room: Room!
    var delegate: FacilityRoomCellDelegate?
    var optionIndex = 0
    let pickeroptions = ["1 Room, 1 Adult", "1 Room, 2 Adults", "2 Room, 2 Adults", "More Options.."]
    //
    @IBOutlet var roomImage: UIImageView!
    @IBOutlet var roomTypeLabel: UILabel!
    @IBOutlet var roomInfoBtn: MDCRaisedButton!
    @IBOutlet var roomDescLabel: UILabel!
    @IBOutlet var roomGuestsLabel: UILabel!
    @IBOutlet var roomBedsLabel: UILabel!
    @IBOutlet var roomBookingLabel: UILabel!
    @IBOutlet var roomAvailabilityLabel: UILabel!
    @IBOutlet var roomPriceLabel: UILabel!
    @IBOutlet var roomRefundLabel: UILabel!
    @IBOutlet var unitsPickerView: UIPickerView!
    @IBOutlet var selectionIndicator: UIView!
    
    
    @IBAction func roomSelectChanged(_ sender: UISwitch) {
        if (sender.isOn){
            self.setSelected(true, animated: true)
            selectionIndicator.layer.backgroundColor = #colorLiteral(red: 0.1059999987, green: 0.6570000052, blue: 0.97299999, alpha: 1)
            
            if let delegate = delegate{
                delegate.didSelectRoom(room: room, optionindex: optionIndex)
            }
        }
        else{
            self.setSelected(false, animated: true)
            selectionIndicator.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            if let delegate = delegate{
                delegate.didUnSelectRoom(room: room)
            }
        }
    }
    
    
    func setRoom(with:Room){
        room = with
        
        //Room Image
        if let roomimage = room.image {
            if let imagelink = roomimage.link{
                roomImage.kf.setImage(with: URL(string: imagelink))
            }
        }
        if let type = room.type{
            roomTypeLabel.text = type
        }
        if let description = room.description{
            roomDescLabel.text = description
        }
        if let beds = room.beds{
            roomBedsLabel.text = "\(beds) bed(s) per room"
        }
        if let available = room.quantity{
            roomAvailabilityLabel.text = "\(available) room(s) available"
        }
    }
}
