//
//  FacilityResultsTableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 24/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//
import Foundation
import UIKit

class FacilityResultsTableViewCell: UITableViewCell {

    @IBOutlet var mainImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var chargesLabel: UILabel!
    
    
}
