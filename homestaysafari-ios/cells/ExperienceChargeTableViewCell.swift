//
//  ExperienceChargeTableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 20/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MaterialComponents


class ExperienceChargeTableViewCell: UITableViewCell {
    //
    var experienceCharge: ExperienceCharge!
    var delegate: ExperienceChargeCellDelegate?
    var noofTickets: Int!
    //
    //Outlets
    @IBOutlet weak var expChargeNameLabel: UILabel!
    @IBOutlet weak var expChargeAmountLabel: UILabel!
    @IBOutlet weak var expChargeDescLabel: UILabel!
    @IBOutlet weak var expChargeTicketsInput: MDCTextField!
    @IBOutlet weak var expChargeBtn: MDCRaisedButton!
    //
    var expChargeTicketsInputController: MDCTextInputControllerOutlined?
    
    func setCharge(charge: ExperienceCharge){
        experienceCharge = charge
        if let name = charge.name {
            expChargeNameLabel.text = name
        }
        else{
            expChargeNameLabel.text = "FREE"
        }
        //
        if let decription = charge.description {
            expChargeDescLabel.text = decription
        }
        else{
            expChargeDescLabel.text = ""
        }
        //
        if let amount = charge.amount {
            var formattedAmount = ""
            if let currency = charge.currency{
                formattedAmount += currency
            }
            formattedAmount += " \(amount)"
            expChargeAmountLabel.text = formattedAmount
        }
        else{
            expChargeAmountLabel.text = "FREE"
            expChargeBtn.isHidden = true
            expChargeTicketsInput.isHidden = true
        }
        expChargeTicketsInputController = MDCTextInputControllerOutlined(textInput: expChargeTicketsInput)
    }
    
    @IBAction func didTouchUInsideSelectBtn(_ sender: Any) {
        var ticketcountstr: String = expChargeTicketsInput.text ?? ""
        if ticketcountstr == "" {
            ticketcountstr = "0"
            expChargeTicketsInput.becomeFirstResponder()
        }
        else{
            let ticketcount: Int! = Int(ticketcountstr)
            delegate?.didSelectEperienceCharge(charge: experienceCharge, count:ticketcount)
        }
        
    }
    
}
