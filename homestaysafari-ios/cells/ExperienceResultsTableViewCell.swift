//
//  ExperienceResultsTableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 19/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class ExperienceResultsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var expResultImage: UIImageView!
    @IBOutlet weak var expResultNameLabel: UILabel!
    @IBOutlet weak var expResultLocationLabel: UILabel!
    @IBOutlet weak var expResultRatingLabel: UILabel!
    @IBOutlet weak var expResultPriceLabel: UILabel!

}
