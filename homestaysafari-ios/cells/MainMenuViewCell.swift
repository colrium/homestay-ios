//
//  MainMenuViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 05/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class MainMenuViewCell: UITableViewCell {
    @IBOutlet weak var menuItemIcon: UIImageView!
    @IBOutlet weak var menuItemLabel: UILabel!
}
