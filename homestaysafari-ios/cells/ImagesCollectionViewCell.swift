//
//  ImagesCollectionViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 24/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
