//
//  CardView.swift
//  homestaysafari-ios
//
//  Created by Collins on 27/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

@IBDesignable
class CardView: UIView {
    
    @objc @IBInspectable var shadowColor: UIColor? = .black
    
    override func layoutSubviews() {
        layer.cornerRadius = 5
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 5)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2);
        layer.shadowOpacity = 0.3
        layer.shadowPath = shadowPath.cgPath
    }
}
