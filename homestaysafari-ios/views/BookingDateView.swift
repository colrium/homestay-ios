//
//  BookingDateView.swift
//  homestaysafari-ios
//
//  Created by Collins on 15/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit

@IBDesignable
class BookingDateView: UIView {
    var dateNameLabel = UILabel()
    fileprivate var contentWrapper = UIView()
    @IBInspectable
    var title : String = "Date" {
        didSet {
            updateView()
        }
    }
    @IBInspectable
    var bookingdate : String = "" {
        didSet {
            updateView()
        }
    }
    @IBInspectable
    var headerColor : UIColor = UIColor.gray {
        didSet {
            updateView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentWrapper.frame = bounds
        contentWrapper.backgroundColor = UIColor.green
        contentWrapper.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentWrapper.clipsToBounds = true
        //
        dateNameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
        dateNameLabel.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        dateNameLabel.text = title
        dateNameLabel.textAlignment = .center
        //
        dateNameLabel.lineBreakMode = .byWordWrapping
        dateNameLabel.font = UIFont(name: "Helvetica-Bold", size: 16)
        dateNameLabel.textColor = UIColor.darkGray
        addSubview(dateNameLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    func updateView(){
        setNeedsLayout()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
