//
//  SearchResultsFacilityTableViewCell.swift
//  homestaysafari-ios
//
//  Created by Collins on 18/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import MaterialComponents

class SearchResultsFacilityTableViewCell: UITableViewCell {

    

    @IBOutlet var imageview: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBAction func vieBtnAction(_ sender: Any) {
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
