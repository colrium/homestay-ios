//
//  UserManager.swift
//  homestaysafari-ios
//
//  Created by Collins on 27/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

class UserDefaultsManager {
    static let userDefaults = UserDefaults.standard
    static func getCurrency()->String{
        if let currency = userDefaults.string(forKey: "currency") {
            return currency
        }
        else{
            return "USD"
        }
        
    }
    
    static func setUserSettings(with userSetting:UserSettings){
        //ToDo: setSettings
    }
    
    static func getUserSettings()->UserSettings{
        let userSettings = UserSettings()
        
        
        return userSettings
    }
    
    static func isloggedin()->Bool{
        return userDefaults.bool(forKey: "isloggedin")
    }
    
    static func toggleLoginStatus(){
    
    }
    
    
}
