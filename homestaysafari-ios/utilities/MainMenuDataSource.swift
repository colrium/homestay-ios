//
//  MainMenuDataSource.swift
//  homestaysafari-ios
//
//  Created by Collins on 06/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

class MainMenuDataSource : NSObject, UITableViewDataSource {
    //
    var sections:[MainMenuSection] = [
        MainMenuSection(with: "EXPLORE THE WORLD", items: ["Start Safari", "Start Experiences", "Overview"], itemsicons: [#imageLiteral(resourceName: "ic_public"), #imageLiteral(resourceName: "ic_golf_course"), #imageLiteral(resourceName: "ic_list")], segueIds:["menuToExplore","menuToExplore","menuToOverview"]),
        MainMenuSection(with: "MY RESERVATIONS", items: ["All Reservations", "Upcoming Reservations", "Recent Reservations", "My Wishlist"], itemsicons: [#imageLiteral(resourceName: "ic_beach_access"), #imageLiteral(resourceName: "ic_update"), #imageLiteral(resourceName: "ic_history"), #imageLiteral(resourceName: "ic_favorite_border")], segueIds:["menuToAllReservations","menuToUpcomingReservations","menuToRecentReservations","menuToWishlist"]),
        MainMenuSection(with: "INVOICES & PAYMENTS", items: ["All Invoices", "Un-Paid Invoices", "My Payments"], itemsicons: [#imageLiteral(resourceName: "ic_account_balance_wallet"), #imageLiteral(resourceName: "ic_shopping_cart"), #imageLiteral(resourceName: "ic_payment")], segueIds:["menuToAllInvoices", "menuToAllUnpaidInvoices", "menuToMyPayments"]),
        MainMenuSection(with: "MISC", items: ["Facility Rating", "Facility Reviews"], itemsicons: [ #imageLiteral(resourceName: "ic_star_half"), #imageLiteral(resourceName: "ic_feedback")], segueIds:["menuToFacilityRating", "menuToFacilityReviews"]),
        MainMenuSection(with: "MY ACCOUNT", items: ["Profile", "Logout"], itemsicons: [#imageLiteral(resourceName: "ic_account_circle"), #imageLiteral(resourceName: "ic_input")], segueIds:["menuToProfile","menuToLogout"])]
    
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].entries.count
    }
    
    
    // Be careful of shenanigans here!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "MainMenuViewCell", for: indexPath) as! MainMenuViewCell
        let cell = Bundle.main.loadNibNamed("MainMenuViewCell", owner: self, options: nil)?.first  as! MainMenuViewCell
        cell.menuItemIcon.image = sections[indexPath.section].entriesIcons[indexPath.row]
        cell.menuItemLabel.text = sections[indexPath.section].entries[indexPath.row]
        cell.menuItemIcon.image = cell.menuItemIcon.image!.withRenderingMode(.alwaysTemplate)
        cell.menuItemIcon.tintColor = UIColor.red
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
}
