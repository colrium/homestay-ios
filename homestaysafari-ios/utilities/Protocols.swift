//
//  Delegates.swift
//  homestaysafari-ios
//
//  Created by Collins on 20/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

protocol SWRevealNavigationDelegate {
    func didSelectNavItem(position:Int, section:MainMenuSection)
    func didSelectUserAvatar(avatarImageView:UIImageView)
}

protocol FacilityRoomCellDelegate {
    func didSelectRoom(room:Room, optionindex:Int)
    func didUnSelectRoom(room:Room)
}

protocol ExperienceChargeCellDelegate {
    func didSelectEperienceCharge(charge:ExperienceCharge, count:Int)
}

protocol BookingDateDelegate {
    func didSelectDate(date:Date, name:String)
}

