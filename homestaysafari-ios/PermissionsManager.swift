//
//  PermissionsManager.swift
//  homestaysafari-ios
//
//  Created by Collins on 17/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import AssetsLibrary


class PermissionsManager: NSObject {
    //MARK:
    static func authorizeLocationWith(comletion:@escaping (Bool)->Void ){
        let granted = CLLocationManager.authorizationStatus()
        switch granted {
        case CLAuthorizationStatus.authorizedAlways,CLAuthorizationStatus.authorizedWhenInUse,CLAuthorizationStatus.authorized:
            comletion(true)
        case CLAuthorizationStatus.denied,CLAuthorizationStatus.restricted:
            comletion(false)
        case CLAuthorizationStatus.notDetermined:
            comletion(false)
        }
    }
    //MARK :
    static func authorizePhotoWith(comletion:@escaping (Bool)->Void )
    {
        let granted = PHPhotoLibrary.authorizationStatus()
        switch granted {
        case PHAuthorizationStatus.authorized:
            comletion(true)
        case PHAuthorizationStatus.denied,PHAuthorizationStatus.restricted:
            comletion(false)
        case PHAuthorizationStatus.notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                comletion(status == PHAuthorizationStatus.authorized ? true:false)
            })
        }
        
    }
    //MARK:
    static func authorizeCameraWith(comletion:@escaping (Bool)->Void )
    {
        let granted = AVCaptureDevice.authorizationStatus(for: AVMediaType.video);
        
        switch granted {
        case AVAuthorizationStatus.authorized:
            comletion(true)
            break;
        case AVAuthorizationStatus.denied:
            comletion(false)
            break;
        case AVAuthorizationStatus.restricted:
            comletion(false)
            break;
        case AVAuthorizationStatus.notDetermined:
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted:Bool) in
                comletion(granted)
            })
        }
    }
    
    //MARK:
    static func jumpToSystemPrivacySetting(){
        let appSetting = URL(string:UIApplication.openSettingsURLString)
        
        if appSetting != nil
        {
            if #available(iOS 10, *) {
                UIApplication.shared.open(appSetting!, options: [:], completionHandler: nil)
            }
            else{
                UIApplication.shared.openURL(appSetting!)
            }
        }
    }
}
