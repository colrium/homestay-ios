//
//  File.swift
//  homestaysafari-ios
//
//  Created by Collins on 30/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import MapKit

struct ApartmentFacility {
    var id: Int?
}

struct AppUser {
    var isLoggedIn: Bool?
    var userName: String?
    var userId: Int?
}


struct BookingWishItem {
    var id: Int?
    var date: Date?
    var recordId: Int?
    var record: Any?
    var type: String?
}


struct CampFacility {
    var id: Int?
}


struct ExchangeRate {
    var id: Int?
    var country: String?
    var code: String?
    var value: CLong?
    var base: String?
    var timestamp: CLong?
    var description: String?
    var created_at: String?
    var updated_at: String?
}

struct FacilityReview:Equatable {
    var id: Int?
    var value: String?
    var Author: String?
    var Country: String?
    var when: Date?
    var AuthorThumb: String?
    var authorId: Int?
    var facilityId: Int?
    var roomId: Int?
}


struct FacilityLocation: Equatable {
    static func == (lhs: FacilityLocation, rhs: FacilityLocation) -> Bool {
        return lhs.id == rhs.id
            && lhs.address == rhs.address
            && lhs.city == rhs.city
            && lhs.country == rhs.country
            && lhs.center == rhs.center
            && lhs.bounds == rhs.bounds
            && lhs.facilities == rhs.facilities
        
    }
    var id: Int?
    var address: String?
    var city: String?
    var country: String?
    var center: CLLocationCoordinate2D?
    var bounds: String?
    var facilities: [HospitalityFacility]?
}

struct FacilityAmenity:Equatable {
    enum AmenityType{
        case SwimmingPool,Gym,Playground,Kitchen,Other
    }
    static func == (lhs: FacilityAmenity, rhs: FacilityAmenity) -> Bool {
        return lhs.id == rhs.id
        && lhs.facility == rhs.facility
        && lhs.name == rhs.name
        && lhs.desc == rhs.desc
        && lhs.type == rhs.type
    }
    var id: Int?
    var facility: HospitalityFacility?
    var name: String?
    var desc: String?
    var type: AmenityType?
}

struct FacilityReservation: Equatable {
    enum BookingType {
        case MySelf,SomeoneElse
    }
    enum BookingPurpose {
        case Leisure,Business,Education
    }
    enum PaymentMode {
        case PayLater,Paypal,Pesapal,Mpesa
    }
    static func == (lhs: FacilityReservation, rhs: FacilityReservation) -> Bool {
        return lhs.id == rhs.id
            //&& lhs.rooms == rhs.rooms
            && lhs.dateBooked == rhs.dateBooked
            && lhs.checkinDate == rhs.checkinDate
            && lhs.checkoutDate == rhs.checkoutDate
            && lhs.adults == rhs.adults
            && lhs.bookingType == rhs.bookingType
            && lhs.children == rhs.children
            && lhs.requireParking == rhs.requireParking
            && lhs.requireSmoking == rhs.requireSmoking
            && lhs.specialRequest == rhs.specialRequest
            && lhs.guestName == rhs.guestName
            && lhs.guestEmail == rhs.guestEmail
            && lhs.coupon == rhs.coupon
            && lhs.purpose == rhs.purpose
            && lhs.paymentMode == rhs.paymentMode
            && lhs.mpesaPaymentCredentials == rhs.mpesaPaymentCredentials
            && lhs.pesapalPaymentCredentials == rhs.pesapalPaymentCredentials
            && lhs.paypalPaymentCredentials == rhs.paypalPaymentCredentials
            && lhs.invoice == rhs.invoice
            && lhs.facility == rhs.facility
    }
    var id: Int?
    //var rooms: [RoomType:Int]?
    var dateBooked: Date?
    var checkinDate: Date?
    var checkoutDate: Date?
    var adults: Int?
    var bookingType: BookingType?
    var children: [Int:Int]?
    var requireParking: Bool?
    var requireSmoking: Bool?
    var specialRequest: String?
    var guestName: String?
    var guestEmail: String?
    var coupon: InvoiceCoupon?
    var purpose: BookingPurpose?
    var paymentMode: PaymentMode?
    var mpesaPaymentCredentials: PaymentCredential?
    var pesapalPaymentCredentials: PaymentCredential?
    var paypalPaymentCredentials: PaymentCredential?
    var invoice: Invoice?
    var facility: HospitalityFacility?
}

struct FacilityRating: Equatable{
    static func == (lhs: FacilityRating, rhs: FacilityRating) -> Bool {
        return lhs.id == rhs.id
            && lhs.userId == rhs.userId
            && lhs.facilityId == rhs.facilityId
            && lhs.roomId == rhs.roomId
            && lhs.value == rhs.value
            && lhs.when == rhs.when
            && lhs.comment == rhs.comment
            && lhs.facility == rhs.facility
            && lhs.room == rhs.room
    }
    var id: Int?
    var userId: Int?
    var facilityId: Int?
    var roomId: Int?
    var value: Double?
    var when: Date?
    var comment: String?
    var facility: HospitalityFacility?
    var room: FacilityRoom?
}

struct FacilityRoom: Equatable {
    enum RoomStatus {
        case OK,Repairs
    }
    static func == (lhs: FacilityRoom, rhs: FacilityRoom) -> Bool {
        return lhs.id == rhs.id
            && lhs.hotel == rhs.hotel
            && lhs.type == rhs.type
            && lhs.status == rhs.status
            && lhs.occupied == rhs.occupied
            && lhs.desc == rhs.desc
            && lhs.bathrooms == rhs.bathrooms
            && lhs.beds == rhs.beds
            && lhs.guests == rhs.guests
            && lhs.name == rhs.name
            && lhs.images == rhs.images
    }
    var id: Int?
    var hotel: HospitalityFacility?
    var type: RoomType?
    var status: RoomStatus?
    var occupied: Bool?
    var desc: String?
    var bathrooms: Int?
    var beds: Int?
    var guests: Int?
    var name: String?
    var images: [String]?
}


struct HospitalityFacility:Equatable {
    enum FacilityType {
        case Hotel,ServicedApartment,Villa,Camp
    }
    static func == (lhs: HospitalityFacility, rhs: HospitalityFacility) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.photo == rhs.photo
            && lhs.type == rhs.type
            && lhs.desc == rhs.desc
            && lhs.location == rhs.location
            && lhs.rated == rhs.rated
            && lhs.rooms == rhs.rooms
            && lhs.amenities == rhs.amenities
            && lhs.images == rhs.images
            && lhs.reviews == rhs.reviews
            && lhs.ratings == rhs.ratings
            && lhs.invoices == rhs.invoices
            && lhs.reservations == rhs.reservations
    }
    var id: Int?
    var name: String?
    var photo: RecordImage?
    var type : FacilityType?
    var desc: String?
    var location: FacilityLocation?
    var rated: Double?
    var rooms: [FacilityRoom]?
    var amenities: [FacilityAmenity]?
    var images: [RecordImage]?
    var reviews: [FacilityReview]?
    var ratings: [FacilityRating]?
    var invoices: [Invoice]?
    var reservations: [FacilityReservation]?
}

struct Invoice:Equatable, Hashable {
    static func == (lhs: Invoice, rhs: Invoice) -> Bool {
        return lhs.id == rhs.id
            && lhs.amount == rhs.amount
            && lhs.currency == rhs.currency
            && lhs.description == rhs.description
            && lhs.dateInvoiced == rhs.dateInvoiced
            && lhs.number == rhs.number
            && lhs.state == rhs.state
            && lhs.commodity == rhs.commodity
            && lhs.commoditytype == rhs.commoditytype
            && lhs.commodityId == rhs.commodityId
            && lhs.payments == rhs.payments
    }
    enum InvoiceState {
        case Pending,Refunded,Cancelled,Paid
    }
    enum CommodityType {
        case FacilityReservation,ExperienceBooking
    }
    var id: Int?
    var amount: Double?
    var currency: String?
    var description: String?
    var dateInvoiced: Date?
    var number: String?
    var state: InvoiceState?
    var commodity: AnyHashable?
    var commoditytype: AnyHashable?
    var commodityId: Int?
    var commoditytypeId: Int?
    var payments: [InvoicePayment]?
}

struct InvoiceCoupon: Equatable, Hashable {
    static func == (lhs: InvoiceCoupon, rhs: InvoiceCoupon) -> Bool {
        return lhs.id == rhs.id
            && lhs.code == rhs.code
            && lhs.value == rhs.value
    }
    var id: Int?
    var code: String?
    var value: Double?
}

struct InvoicePayment:Equatable, Hashable {
    enum PaymentStatus {
        case Failed,Success
    }
    enum PaymentType {
        case Paypal,Pesapal,Mpesa
    }
    var id: Int?
    var invoiceId: Int?
    var reference: String?
    var amount: Double?
    var datePaid: Date?
    var dateCreated: Date?
    var type: PaymentType?
    var currency: String?
    var number: String?
    var description: String?
    var status: PaymentStatus?
    var invoice: Invoice?
}

struct PaymentCredential: Equatable, Hashable {
    static func == (lhs: PaymentCredential, rhs: PaymentCredential) -> Bool {
        return lhs.userName == rhs.userName
            && lhs.userEmail == rhs.userEmail
            && lhs.mpesaPhone == rhs.mpesaPhone
            && lhs.account == rhs.account
            && lhs.paymentId == rhs.paymentId
            && lhs.redirectUrl == rhs.redirectUrl
    }
    var userName: String?
    var userEmail: String?
    var mpesaPhone: String?
    var account: String?
    var paymentId: Int?
    var redirectUrl: String?
}



struct RoomPrice:Equatable {
    enum PriceType {
        case Daily,Weekly,Monthly
    }
    //
    enum Package {
        case Full_Board,Half_Board,Bed_and_Breakfast
    }
    static func == (lhs: RoomPrice, rhs: RoomPrice) -> Bool {
            return lhs.id == rhs.id
            && lhs.roomType == rhs.roomType
            && lhs.type == rhs.type
            && lhs.package == rhs.package
            && lhs.amount == rhs.amount
            && lhs.currency == rhs.currency
    }
    var id: Int?
    var roomType: RoomType?
    var type: PriceType?
    var package: Package?
    var amount: Double?
    var currency: String?
}

struct RoomType: Equatable{
    static func == (lhs: RoomType, rhs: RoomType) -> Bool {
            return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.category == rhs.category
            && lhs.cancellationDays == rhs.cancellationDays
            && lhs.desc == rhs.desc
            && lhs.caption == rhs.caption
            && lhs.beds == rhs.beds
            && lhs.guests == rhs.guests
            && lhs.bathrooms == rhs.bathrooms
            && lhs.hasWifi == rhs.hasWifi
            && lhs.hasEntertainment == rhs.hasEntertainment
            && lhs.misc == rhs.misc
            && lhs.area == rhs.area
            && lhs.rated == rhs.rated
            && lhs.ratingComment == rhs.ratingComment
            && lhs.bedSize == rhs.bedSize
            && lhs.entAmenities == rhs.entAmenities
            && lhs.facility == rhs.facility
            && lhs.prices == rhs.prices
            && lhs.rooms == rhs.rooms
            && lhs.reviews == rhs.reviews
    }
    
    enum Category {
        case Standard,KingRoom,SeaView
    }
    var id: Int?
    var name: String?
    var category: Category?
    var cancellationDays: Int?
    var cancellationCharges: Double?
    var desc: String?
    var caption: Int?
    var beds: Int?
    var guests: Int?
    var bathrooms: Int?
    var hasWifi: Bool?
    var hasEntertainment: Bool?
    var misc: String?
    var area: String?
    var rated: Double?
    var ratingComment: String?
    var bedSize: String?
    var entAmenities: [String]?
    var facility: HospitalityFacility?
    var prices: [RoomPrice]?
    var rooms: [FacilityRoom]?
    var reviews: [FacilityReview]?
    var images: [RecordImage]?
}


struct UserSettings {
    var currency: String?
    var language: String?
    var applySearchRadius: Bool?
    var searchRadius: Double?
}



