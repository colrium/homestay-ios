//
//  MainMenuSection.swift
//  homestaysafari-ios
//
//  Created by Collins on 06/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

struct MainMenuSection {
    var title: String
    var entries: [String]
    var entriesIcons: [UIImage]
    var segueIIdentifiers: [String]
    
    init(with heading:String, items:[String], itemsicons:[UIImage], segueIds:[String]) {
        title = heading
        entries = items
        entriesIcons = itemsicons
        segueIIdentifiers = segueIds
    }
}
