//
//  FacilitySearchParams.swift
//  homestaysafari-ios
//
//  Created by Collins on 26/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

struct FacilitySearchParams {
    var query: String?
    var children: Int?
    var adults: Int?
    var rooms: Int?
    var checkindate: Date?
    var checkoutdate: Date?
}
