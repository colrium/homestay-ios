//
//  MapMarker.swift
//  homestaysafari-ios
//
//  Created by Collins on 17/10/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation
import MapKit
import UIKit
import Contacts

class MapMarker: NSObject, MKAnnotation {
    var title: String?
    var locationName: String?
    var discipline: String?
    var coordinate: CLLocationCoordinate2D
    
    init(center:CLLocationCoordinate2D){
        coordinate = center
        //
        super.init()
    }
    
    convenience init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        //
        self.init(center:coordinate)
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
    }
    
    var subtitle: String? {
        return locationName
    }
    // Annotation right callout accessory opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle ?? "Loading..."]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
