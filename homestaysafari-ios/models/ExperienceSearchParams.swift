//
//  ExperienceSearchParams.swift
//  homestaysafari-ios
//
//  Created by Collins on 26/11/2018.
//  Copyright © 2018 churchblaze. All rights reserved.
//

import Foundation

struct ExperienceSearchParams {
    var query: String?
    var checkinDate: Date?
    var checkoutDate: Date?
}
